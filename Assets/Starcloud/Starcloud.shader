Shader "Starcloud"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Amount("Amount",float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
          
            #include "UnityCG.cginc"
            #include "../XCGInclude.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
               
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Amount;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
               
                return o;
            }
            float fbm(float3 p, float t) {

                float freq = .8 + .3 * cos(_Time.y * .1);
                //float freq = 1. + .4 * sin(t+_Time.y *.1);
                //float amp = .8 + .3 *sin(t+_Time.y * .1);
                float amp = .96 + .09 * sin(_Time.y * .167);

                float v = 0.;
                for (int i = 0; i < 6; i++) {
                    float x = float(i) * .01;
                    float y = cos(_Time.y) * .1;
                    float z = sin(_Time.y) * .02;
                    float2x2 R = float2x2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    float3 p2 = float3(p.x, p.y, p.z);
                    p2.xz= mul(p2.xz, R);
                    p2.xy =mul(p2.xy, R);
                    p2.yz =mul(p2.yz, R);
                    v += amp * noise3d(p2 * freq);
                    freq *= 2.;
                    amp *= .5;
                }
                return v;
            }


            float Star(float2 p) {
                float l = length(p);
                float c = 0.02 / l;
                c *= smoothstep(1., 0.0, l);
                return c;
            }
            float StarLayer(float2 uv, float3 w, float3 v, float f) {
                float col = 0.;
                float xls = 3.;
                float yls = 3.;

                float2 i_uv = floor(uv);
                float2 f_uv = frac(uv);
                float2 mv = 0;
                for (float y = -yls; y <= yls; y++) {
                    for (float x = -xls; x <= xls; x++) {
                        float2 off = float2(x, y);
                        float2 mv = 0;
                        float r = frac(rand2d(i_uv + off + 22.2) * 224.89);
                        mv.x = r;
                        mv.y = frac(r * 66.9642);
                        float sv = Star(f_uv - off - mv + (f * f * w.x * v.y * w.y * v.x * f * f) * .5);
                        col += sv;
                    }
                }
                return col;

            }

            fixed4 frag (v2f i) : SV_Target
            {
                i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
                float3 col = 0;
                float3 v = 0;
                float3 w = 0;
                float t = _Time.y *0.001;
                t += 120.;
                float2 uv2 = mul(uv, rot2d(_Time.y));
                float num_l = 10.;
                for (float z = 0.; z < 1.; z += 1. / num_l) {
                    float3 p2 = 0;
                    p2.x = uv.x;
                    p2.y = uv.y;
                    float d = frac(t + z);
                    float dz = lerp(20., 1., d);
                    p2.z = dz;

                    v.x = fbm(p2 + sin(_Time.y) * .1, dz);
                    v.y = fbm(p2 + v.x + cos(_Time.y) * .1, dz);
                    v.z = fbm(p2 + v.y, dz);
                    w.x = fbm(p2 + v.x, dz);
                    w.y = fbm(p2 + w.x, dz);
                    w.z = fbm(p2 + v.y, dz);

                    float fade = d * smoothstep(1., .99, d);
                    float f = fbm(w + v, dz);

                    col.r += Star(float2(f * f * f * f, p2.y));
                    col.g += Star(float2(p2.x, f * f * f * f * f));
                    col.b += Star(float2(w.x, v.y));

                    float d2 = frac(z + _Time.y * .1);
                    float dd = d2 * smoothstep(1., .90, d2);  // starlayer distance          

                    float ss = lerp(10., .1, d2);
                    float2 p = uv * ss;
                    col += StarLayer(p + z * 29., w, v, f) * dd * smoothstep(.4, 1., frac(_Time.y * .3)) * smoothstep(1., .7, frac(_Time.y * .3));

                    col.g += f * f * f * f * f * .069 * fade;
                    col.b += distance(w * f * f * f, float3(f, uv2.x * f * f, uv2.y)) * .14 * fade;
                    col.r += distance(v, float3(w.z, uv2.x * f * f, f * f * f * f)) * .1 * fade;
                }
                col = lerp(0., col, _Amount);

                return fixed4(col, 1);
            }
            ENDCG
        }
    }
}
