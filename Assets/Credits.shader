Shader "Credits"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            #include "XCGInclude.cginc"
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
            
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            #define MAX_STEPS 100
            #define MAX_DIST 100.
            #define SURF_DIST .001
            #define PI 3.14159
            struct RMHit {
                float d;
                int id;
            };
            float dstC(float3 p) {
                p.xz = mul(p.xz, rot2d(PI));
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(-1.56));
                float d = dstCappedTorus(p2, float2(1. , .0), .1 , .05 );
                return d;

            }
            float dstO(float3 p) {
                float3 p2 = p;
                p2.yz = mul(p2.yz, rot2d(-1.56));
                float d = dstTorus(p2, float2(.11 , .05 ));
                return d;
            }

            float dstD(float3 p) {
                p.xz = mul(p.xz, rot2d(PI));
                float d = dstVerticalCapsule(p + float3(-0.02, .1 , 0), .2 , .05 );
                float3 p2 = p - float3(.02 , 0, 0);
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstCappedTorus(p2, float2(1. , .0), .1 , .05 ), .001 );
                return d;
            }

            float dstE(float3 p) {
                p.xz = mul(p.xz, rot2d(PI));
                float3 p2 = p;
                float d = dstVerticalCapsule(p + float3(-0.01, .1 , 0), .18 , .05 );

                p2.y -= .1 ;
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstVerticalCapsule(p2, .15 , .05 ), .01 );
                p2 = p;
                p2.x -= 0.01;

                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstVerticalCapsule(p2, .1 , .05 ), .01 );
                p2 = +p;
                p2.x -= 0.01;
                p2.y += .1 ;
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstVerticalCapsule(p2, .15 , .05 ), .01 );
                return d;

            }

            float dstSemiC(float3 p) {
                float d = dstSphere(p - float3(0, .05 , 0), .05 );
                d = min(d, dstSphere(p + float3(0, .05 , 0), .05 ));
                return d;
            }

            float dstX(float3 p) {
                p.xz = mul(p.xz, rot2d(PI));
                float3 p2 = p;
                p2.y += .1 ;
                p2.xy = mul(p2.xy, rot2d(.6));
                float d = dstVerticalCapsule(p2, .24 , .05 );
                p2 = p;
                p2.y += .1 ;
                p2.x += .27;
                p2.x -= .13 ;
                p2.xy = mul(p2.xy, rot2d(-.6));
                d = smin(d, dstVerticalCapsule(p2, .24 , .05 ), .01 );
                return d;
            }

            float dstR(float3 p) {
                p.xz = mul(p.xz, rot2d(PI));
                float d = dstVerticalCapsule(p + float3(-0.08, .1 , 0), .2 , .05 );
                float3 p2 = p - float3(.04 , 0.05 , 0);
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstCappedTorus(p2, float2(1. , .0), .05 , .05 ), .001 );
                p2 = p;
                p2.x += .025;
                p2.y += .1;
                p2.xy = mul(p2.xy, rot2d(-.6));
                d = smin(d, dstVerticalCapsule(p2, .1 , .05 ), 0.01 );
                return d;
            }

            float dstG(float3 p) {
                p.xz = mul(p.xz, rot2d(PI));
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(-1.56));
                float d = dstCappedTorus(p2, float2(1. , .0), .1 , .05 );
                p2 = p;
                p2.y += .1;
                p2.x += .03;
                d = smin(d, dstVerticalCapsule(p2, .05 , .05 ), 0.01 );
                p2 = p;
                p2.y += .02 ;
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstVerticalCapsule(p2, .03 , .05 ), 0.01 );
                p2 = p;
                p2.y -= .1;
                
                p2.xy = mul(p2.xy, rot2d(1.6));
                d = smin(d, dstVerticalCapsule(p2, .04 , .05 ), 0.01 );
                return d;
            }

            float dstM(float3 p) {
                float d = dstVerticalCapsule(p + float3(0, .1 , 0), .2 , .05 );
                float3 p2 = p;
                p2.y += .04 ;
                p2.x -= .1 ;
                p2.xy = mul(p2.xy, rot2d(-.6));
                d = smin(d, dstVerticalCapsule(p2, .16 , .05 ), 0.01 );
                p2 = p;
                p2.y += .04 ;
                p2.x -= .1 ;
                p2.xy = mul(p2.xy, rot2d(.6));
                d = smin(d, dstVerticalCapsule(p2, .16 , .05 ), 0.01 );
                d = smin(d, dstVerticalCapsule(p + float3(-.2 , .1 , 0), .16 , .05 ), 0.01 );
                return d;
            }

            float dstU(float3 p) {
                float3 p2 = p;
                p2.y += .02;
                p2.xy = mul(p2.xy, rot2d(PI));
                float d = dstCappedTorus(p2, float2(1. , .0), .08 , .05 );
                d = smin(d, dstVerticalCapsule(p + float3(0.08 , .03 , 0), .12 , .05 ), 0.01 );
                d = smin(d, dstVerticalCapsule(p + float3(-0.08 , .03 , 0), .12 , .05 ), 0.01 );
                return d;
            }
            float dstS(float3 p) {
                p.xz = mul(p.xz, rot2d(PI));
                float3 p2 = p;
                p2.y -= .05 ;
                p2.xy = mul(p2.xy, rot2d(-1.56));
                float d = dstCappedTorus(p2, float2(1. , -.2 ), .0525 , .04 );
                p2 = p;
                p2.x -= .03 ;
                p2.y += .05 ;
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstCappedTorus(p2, float2(1. , -.2 ), .0525 , .04 ), 0.02 );
                return d;
            }

            float dstI(float3 p) {
                float d = dstVerticalCapsule(p + float3(0, .1 , 0), .08 , .05 );
                d = min(d, dstSphere(p - float3(0, .1 , 0), .05 ));
                return d;
            }

            float dstK(float3 p) {
                p.xz = mul(p.xz, rot2d(PI));
                float d = dstVerticalCapsule(p + float3(0, .1 , 0), .2 , .05 );
                float3 p2 = p;
                p2.x += .12;
                p2.y += .1;
                p2.xy = mul(p2.xy, rot2d(-.8));
                d = smin(d, dstVerticalCapsule(p2, .14 , .05 ), 0.01 );
                p2 = p;
                p2.x += .0;
                p2.xy = mul(p2.xy, rot2d(.8));
                d = smin(d, dstVerticalCapsule(p2, .14 , .05 ), 0.01 );
                return d;
            }
            float dstN(float3 p) {
                p.xz = mul(p.xz, rot2d(PI));
                float d = dstVerticalCapsule(p + float3(0, .1 , 0), .2 , .05 );
                float3 p2 = p;
                p2.x -= 0. ;
                p2.y += .1 ;
                p2.xy = mul(p2.xy, rot2d(-.6));
                d = smin(d, dstVerticalCapsule(p2, .24 , .05 ), 0.01 );
                d = smin(d, dstVerticalCapsule(p - float3(.16 , -.1 , 0), .2 , .05 ), 0.01 );
                return d;
            }
            float dstL(float3 p) {
                float d = dstVerticalCapsule(p + float3(0, .1 , 0), .2 , .05 );
                float3 p2 = p;
                //p2.x += .12 ;
                p2.y += .1 ;
                p2.xy = mul(p2.xy, rot2d(-1.56));
                d = smin(d, dstVerticalCapsule(p2, .1 , .05 ), 0.01 );
                return d;
            }
            float dstV(float3 p) {
                float3 p2 = p;
                p2.y += .1 ;
                p2.xy = mul(p2.xy, rot2d(-.4));
                float d = dstVerticalCapsule(p2, .23 , .05 );
                p2 = p;
                p2.y += .1 ;
                p2.xy = mul(p2.xy, rot2d(.4));
                d = smin(d, dstVerticalCapsule(p2, .23 , .05 ), 0.01 );
                return d;

            }

            float dstW(float3 p) {
                float3 p2 = p;
                p2.y += .1 ;
                p2.xy = mul(p2.xy, rot2d(-.4));
                float d = dstVerticalCapsule(p2, .23 , .05 );
                p2 = p;
                p2.y += .1 ;
                p2.xy = mul(p2.xy, rot2d(.4));
                d = smin(d, dstVerticalCapsule(p2, .23 , .05 ), 0.01 );
                p2 = p;
                p2.y += .1 ;
                p2.x -= .2 ;
                p2.xy = mul(p2.xy, rot2d(-.4));
                d = smin(d, dstVerticalCapsule(p2, .23 , .05 ), 0.01 );
                p2 = p;
                p2.y += .1 ;
                p2.x -= .2 ;
                p2.xy = mul(p2.xy, rot2d(.4));
                d = smin(d, dstVerticalCapsule(p2, .23 , .05 ), 0.01 );
                return d;
            }

            float dstA(float3 p) {
                p.xz = mul(p.xz, rot2d(PI));
                float3 p2 = p;
                p2.y += .12 ;
                p2.xy = mul(p2.xy, rot2d(.4));
                float d = dstVerticalCapsule(p2, .24 , .05 );
                p2 = p;
                p2.y += .12 ;
                p2.x += .2 ;
                p2.xy = mul(p2.xy,rot2d(-.4));
                d = smin(d, dstVerticalCapsule(p2, .24 , .05 ), 0.01 );
                p2 = p;
                p2.y += .06 ;
                p2.x += .05 ;
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstVerticalCapsule(p2, .1 , .05 ), 0.01 );

                return d;
            }

            float dstSwallow(float3 p) {
                float d = dstS(p + float3(1. , 0, 0));
                d = min(d, dstW(p + float3(.75 , 0, 0)));
                d = min(d, dstA(p + float3(.375 , 0, 0)));
                d = min(d, dstL(p + float3(.05 , 0, 0)));
                d = min(d, dstL(p - float3(.2, 0, 0)));
                d = min(d, dstO(p - float3(.55, 0, 0)));
                d = min(d, dstW(p - float3(.9, 0, 0)));
                return d;
            }
            float dstSLove(float3 p) {
                float d = dstS(p + float3(1.5 , 0, 0));
                d = min(d, dstK(p + float3(1.35 , 0, 0)));
                d = min(d, dstI(p + float3(1.1 , 0, 0)));
                d = min(d, dstN(p + float3(.7825 , 0, 0)));
                d = min(d, dstL(p + float3(.65 , 0, 0)));
                d = min(d, dstE(p + float3(.4 , 0, 0)));
                d = min(d, dstS(p + float3(.075 , 0, 0)));
                d = min(d, dstS(p - float3(.1 , 0, 0)));
                d = min(d, dstL(p - float3(.4 , 0, 0)));
                d = min(d, dstO(p - float3(.75 , 0, 0)));
                d = min(d, dstV(p - float3(1.1 , 0, 0)));
                d = min(d, dstE(p - float3(1.35 , 0, 0)));
                return d;
            }
            float dstMusic(float3 p) {
                float d = dstM(p + float3(.6 , 0, 0));
                d = min(d, dstU(p + float3(.175 , 0, 0)));
                d = min(d, dstS(p + float3(-.075 , 0, 0)));
                d = min(d, dstI(p + float3(-.225 , 0, 0)));
                d = min(d, dstC(p + float3(-.45 , 0, 0)));
                d = min(d, dstSemiC(p + float3(-.55 , 0, 0)));
                return d;
            }

            float dstXreg(float3 p) {
                float d = dstX(p + float3(.45 , 0., 0.));
                d = min(d, dstR(p + float3(.1 , 0, 0)));
                d = min(d, dstE(p - float3(.075 , 0, 0)));
                d = min(d, dstG(p - float3(.45 , 0, 0)));
                return d;
            }
            float dstCode(float3 p) {
                float d = dstC(p + float3(.5 , 0., 0.));
                d = min(d, dstO(p + float3(.25 , 0., 0.)));
                d = min(d, dstD(p - float3(.0, 0., 0.)));
                d = min(d, dstE(p - float3(.25 , 0., 0.)));
                d = min(d, dstSemiC(p - float3(.5 , 0., 0.)));
                return d;
            }
            float3 TransformCode(float3 p) {
                
                p -= float3(0.5, .5, 0.);
                /*p.yz = mul(p.yz, rot2d(_Time.y * .79));
                p.xy = mul(p.xy, rot2d(_Time.y * .45));
                p.zx = mul(p.zx, rot2d(_Time.y * .35));*/
                return p;
            }
            float3 TransformXreg(float3 p) {
                p -= float3(0., .3, 0.);
                /*p.yz = mul(p.yz, rot2d(_Time.y * .49));
                p.xy = mul(p.xy, rot2d(_Time.y * .15));
                p.zx = mul(p.zx, rot2d(_Time.y * .65));*/
                return p;
            }

            float3 TransformMusic(float3 p)
            {
                p -= float3(-0.5, -.5, 0.);
                /*p.yz = mul(p.yz, rot2d(_Time.y * .29));
                p.xy = mul(p.xy, rot2d(_Time.y * .95));
                p.zx = mul(p.zx, rot2d(_Time.y * .33));*/
                return p;
            }

            float3 TransformSLove(float3 p)
            {
                p -= float3(0., 0., 1.);
                /*p.yz = mul(p.yz, rot2d(_Time.y * .59));
                p.xy = mul(p.xy, rot2d(_Time.y * 1.25));
                p.zx = mul(p.zx, rot2d(_Time.y * .44));*/
                return p;
            }


            float3 TransformSwallow(float3 p)
            {
                p -= float3(0., 0., 0.);
                /*p.yz *= rot2d(_Time.y * .79);
                p.xy *= rot2d(_Time.y * .25);
                p.zx *= rot2d(_Time.y * .25);*/
                return p;
            }
            float dstBgr(float3 p) {
                float d = p.z - 20;
                return d;

            }
            RMHit GetDist(float3 p) {
                RMHit hit;
                float code = dstCode(TransformCode(p));
                float xreg = dstXreg(TransformXreg(p));
                float music = dstMusic(TransformMusic(p));
                float slove = dstSLove(TransformSLove(p));
                float swallow = dstSwallow(TransformSwallow(p));
                float ss = lerp(slove, swallow,0.);
                ss = slove;
                //float bgr = dstBgr(p);
                /*float3 p2 = normalize(float3(1,2.,1));
                p2 = float3(4, 2, 2);
               
                hit.d = bgr;
                *///hit.d = min(hit.d, bgr);
                //hit.d = smin(hit.d, xreg, .1);
                float p2 = float3(8, 4, 2);
                float bgr = dstBox(p + float3(0, 0, -10), p2);
                hit.d = min(bgr, xreg);
                hit.d = smin(hit.d, music,.1);
                hit.d = smin(hit.d, ss,.1);
                hit.d = smin(hit.d, code, .1);
                hit.id = 3;
                //if (hit.id = slove) hit.id = 1;
                if (hit.d == bgr) hit.id = 2;
                return hit;
                
            }
            RMHit GetDist2(float3 p) {
                RMHit hit;
                hit.d = dstSwallow(TransformSwallow(p));                
                return hit;
            }

            RMHit RayMarch(float3 ro, float3 rd) {
                float dO = 0.;
                RMHit hit;
                for (int i = 0; i < MAX_STEPS; i++) {
                    float3 p = ro + rd * dO;
                    hit = GetDist(p);
                    dO += hit.d;
                    if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;
                }   
                hit.d = dO;
                return hit;
            }

            float3 GetNormal(float3 p) {
                float d = GetDist(p).d;
                float2 e = float2(0.01, 0);

                float3 n = d - float3(
                    GetDist(p - e.xyy).d,
                    GetDist(p - e.yxy).d,
                    GetDist(p - e.yyx).d);
                return normalize(n);

            }
            RMHit RayMarch2(float3 ro, float3 rd) {
                float dO = 0.;
                RMHit hit;
                for (int i = 0; i < MAX_STEPS; i++) {
                    float3 p = ro + rd * dO;
                    hit = GetDist2(p);
                    dO += hit.d;
                    if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;
                }
                hit.d = dO;
                return hit;
            }

            float3 GetNormal2(float3 p) {
                float d = GetDist2(p).d;
                float2 e = float2(0.01, 0);

                float3 n = d - float3(
                    GetDist2(p - e.xyy).d,
                    GetDist2(p - e.yxy).d,
                    GetDist2(p - e.yyx).d);
                return normalize(n);

            }
            float GetLight(float3 p, float3 lightPos) {

                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p);
                float dif = clamp(dot(n, l), 0., 1.);
                float d = RayMarch(p + n * SURF_DIST * 3., l).d;
                if (d<length(lightPos-p)) dif *= .1; // shadow
                return dif;
            }

            float GetLight2(float3 p, float3 lightPos) {

                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p);
                float dif = clamp(dot(n, l), 0., 1.);
                float d = RayMarch(p + n * SURF_DIST * 3., l).d;
                if (d < length(lightPos - p)) dif *= .1; // shadow
                return dif;
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);            
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {                
                i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;                
                float3 col = 0;                
                float3 ro = float3(0, 0, -2);

                float3 rd = normalize(float3(uv.x, uv.y, 1));
                RMHit hit = RayMarch(ro, rd);
                
                if (hit.d < MAX_DIST) {
                    float3 p = ro + rd * hit.d;
                    if ((hit.id == 3)) {
                        
                        float3 n = GetNormal(p);
                        float3 r = reflect(rd, n);
                        float spec = pow(max(0., r.y), 30.);
                        //if (hit.id == 1) BOX MAT
                        //if (hit.id == 2) SPHHERE MAT
                        float dif = dot(n, normalize(float3(1, 2, 3))) * .5 + .5;
                        //float l = GetLight(p, float3(sin(_Time.y) * 5.,0,cos(_Time.y) * 2.));
                        float l = GetLight(p, float3(0, 0, -1));
                        //float3 c = texture(iChannel0, p.xz).rgb;
                        col = lerp(1., l, .5) + spec;
                    }
                    //if (hit.id == 1) {
                    //    float3 p = ro + rd * hit.d;
                    //    float3 n = GetNormal2(p);
                    //    float3 r = reflect(rd, n);
                    //    float spec = pow(max(0., r.y), 30.);
                    //    //if (hit.id == 1) BOX MAT
                    //    //if (hit.id == 2) SPHHERE MAT
                    //    float dif = dot(n, normalize(float3(1, 2, 3))) * .5 + .5;
                    //    //float l = GetLight(p, float3(sin(_Time.y) * 5.,0,cos(_Time.y) * 2.));
                    //    float l = GetLight2(p, float3(0, 0, -1));
                    //    //float3 c = texture(iChannel0, p.xz).rgb;
                    //    col = lerp(1., l, .5) + spec;

                    //}
                    if (hit.id == 2) {
                        //col = float3(.5, .5, .5);
                        p *= .25;
                        col = tex2D(_MainTex, p.zy).rgb;
                    }
                }
                return fixed4(col, 1);
            }
            ENDCG
        }
    }
}
