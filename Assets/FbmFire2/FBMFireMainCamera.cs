﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FBMFireMainCamera : SceneCameraBase
{
    public GameObject bgr;
    int fadeState = 0;
    Material mat = null;
    //private float timer = 0;
    public float amount = 0;
    public Vector4 cpos, xpos, mpos, slpos, swpos;
    public Vector4 cposTar, xposTar, mposTar, slposTar, swposTar, swposTar2;
    public float speed = 1.0f;
    int swState = 0;
    public float smin = 1f;
    void Start()
    {
        base.Start();
        mat = bgr.GetComponent<Renderer>().material;
        mat.SetFloat("_Amount", 0f);
        mat.SetFloat("_SMin", smin);
        cpos = new Vector4(3.5f, -1.1f, -2, 0);
        cposTar = new Vector4(1.5f, -1.1f, -2, 0);
        xpos = new Vector4(3.5f, -1f, -2, 0);
        xposTar = new Vector4(1.0f, -.9f, -2, 0);
        mpos = new Vector4(6.5f, 0, -3, 0);
        mposTar = new Vector4(1.6f, -.3f, -2, 0);
        
        slpos =     new Vector4(6,  -.1f,-2, 0);
        slposTar =  new Vector4(0f,-.1f, -2, 0);
        swpos =     new Vector4(6,  -.1f, -2, 0);
        swposTar =  new Vector4(0f, -.1f, -2, 0);
        swposTar2 = new Vector4(0f,1f, -52, 0);
        mat.SetVector("_CPos", cpos);
        mat.SetVector("_XPos", xpos);
        mat.SetVector("_MPos", mpos);
        mat.SetVector("_SLPos", slpos);
        mat.SetVector("_SWPos", swpos);
        cpos = new Vector4(0, 0, -2, 0);
        xpos = new Vector4(0, 0, -2, 0);
        mpos = new Vector4(0, 0, -2, 0);
        slpos = new Vector4(0, 0, -2, 0);
        swpos = new Vector4(0, 0, -2, 0);
        mat.SetVector("_CPos", cpos);
        mat.SetVector("_XPos", xpos);
        mat.SetVector("_MPos", mpos);
        mat.SetVector("_SLPos", slpos);
        mat.SetVector("_SWPos", swpos);
    }

    float step = 0;
    
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > 4)
        {
            step = speed * Time.deltaTime; // calculate distance to move
            if (smin > .1f) smin -= Time.deltaTime * .1f;
            mat.SetFloat("_SMin", smin);
            step *= .25f;
            cpos = Vector4.MoveTowards(cpos, cposTar, step);
            mat.SetVector("_CPos", cpos);
            xpos = Vector4.MoveTowards(xpos, xposTar, step);
            mat.SetVector("_XPos", xpos);
            mpos = Vector4.MoveTowards(mpos, mposTar, step);
            mat.SetVector("_MPos", mpos);
            slpos = Vector4.MoveTowards(slpos, slposTar, step);
            mat.SetVector("_SLPos", slpos);

           
            if (swState == 0)
            {
                swpos = Vector4.MoveTowards(swpos, swposTar, step);
                mat.SetVector("_SWPos", swpos);

                if (Vector4.Distance(swpos, swposTar) < 0.1f)
                {
                    swState++;
                }

            }
            else
            {
                swpos = Vector4.MoveTowards(swpos, swposTar2, step);
                mat.SetVector("_SWPos", swpos);

            }
            if (timer>SceneTimes.FbmFire2Scene - 4f)
            {
                if (smin < 1f) smin += Time.deltaTime * .5f;
                mat.SetFloat("_SMin", smin);
            }
        }
        if (fadeState == 0)
        {

            if (timer > 0)
            {
                fadeState++;
            }
        }
        if (fadeState == 1)
        {
            amount += Time.deltaTime / 2f;
            if (amount >= 1.0f) amount = 1.0f;
            mat.SetFloat("_Amount", amount);
            if (timer > 4)
            {
                amount = 1.0f;
                mat.SetFloat("_Amount", amount);
                fadeState++;
            }
        }
        if ((fadeState == 2) && (timer > (SceneTimes.FbmFire2Scene - 0.5f)))
        {
            fadeState++;
        }
        if (timer > SceneTimes.FbmFire2Scene)
        {
            SceneManager.LoadScene("BlackSunScene");
        }
        if (fadeState == 3)
        {
            amount -= Time.deltaTime *2f;
            
            mat.SetFloat("_Amount", amount);
        }
    }
}
