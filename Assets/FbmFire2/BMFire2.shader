﻿Shader "FBMFire2"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Amount("Amount",float) = 0
		_SMin("Smin",float) = 1
		_CPos("Code Pos", Vector) = (0,0,2,0)
		_XPos("Xreg Pos", Vector) = (10,0,1,0)
		_MPos("Music Pos", Vector) = (10,0,1,0)
		_SL("Slove Pos", Vector) = (10,0,1,0)
		_SW("Swallow Pos", Vector) = (10,0,1,0)
	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }
	
		GrabPass { "_GrabTexture" }
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "../XCGInclude.cginc"
			

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 grabUv : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _GrabTexture;
			
			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.grabUv = ComputeGrabScreenPos(o.vertex);
				return o;
			}
						
			
		
							
			float fbmOLD(float2 p) {

				float freq = 1.6 + 1. * sin(_Time.y * .71);
				float amp = 1.0;
				float v = 0.;
				for (int i = 0; i < 6; i++) {
					float x = float(i); // sin(_Time.y * 4);//float(i);
					float y = cos(_Time.y * 3) * sin(_Time.y * 3.79);
					//mat2 R = mat2(cos(.5), sin(.5), -sin(.5), cos(.5));
					float2x2 R = float2x2(cos(.5), -sin(.5), sin(.5), cos(.5));
					p.xy *= mul(p, R);
					v += amp * noise2d(x + y +p * freq);
					
					freq *= 20.5 + 15 * sin(_Time.y * .37); // 2
					amp *= .5; // .5
				}
				return v;
			}


			float fbm(float2 p) {

				float freq = 1.6 + 1. * sin(_Time.y * .71);
				float amp = 1.0;
				float v = 0.;
				for (int i = 0; i < 6; i++) {
					float x = float(i); // sin(_Time.y * 4);//float(i);
					float y = cos(_Time.y * 3) * sin(_Time.y * 3.79);
					//mat2 R = mat2(cos(.5), sin(.5), -sin(.5), cos(.5));
					float2x2 R = float2x2(cos(.5), -sin(.5), sin(.5), cos(.5));
					p.xy *= mul(p, R);
					v += amp * noise2d(x + y + p * freq);

					freq *= 5.5 + 3 * sin(_Time.y * .37); // 2
					amp *= .5; // .5
				}
				return v;
			}

			float fbm2(float2 p) {

				float freq = 1.6 + 1. * sin(_Time.y * .71);
				float amp = 1.0;
				float v = 0.;
				for (int i = 0; i < 6; i++) {
					float x = float(i); // sin(_Time.y * 4);//float(i);
					float y = cos(_Time.y * 3) * sin(_Time.y * 3.79);
					//mat2 R = mat2(cos(.5), sin(.5), -sin(.5), cos(.5));
					float2x2 R = float2x2(cos(.5), -sin(.5), sin(.5), cos(.5));
					p.xy *= mul(p, R);
					v += amp * noise2d(x + y + p * freq);

					freq *= 20.5 + 15 * sin(_Time.y * .7); // 2
					amp *= .5; // .5
				}
				return v;
			}

			float Star(float2 p) {
				float l = length(p);
				float c = .1 / l;
				c *= smoothstep(1., 0.1, l);

				return c;
			}

			float3 randomStar(float2 uv) {
				uv *= 25.;
				float2 f_uv = frac(uv);
				float2 i_uv = floor(uv);
				float3 col = 0;


				int ls = 2;
				float c = 0;
				for (int y = -ls; y <= ls; y++) {
					for (int x = -ls; x <= ls; x++) {

						float2 off = float2(x, y);
						float s = rand2d(i_uv + off);
						float2 mv = float2(rand2d((i_uv + off) * 242.49), rand2d(i_uv + off));
						mv.x *= 4. * sin(_Time.y * rand2d(i_uv + off));
						mv.y *= 4. * cos(_Time.y * rand2d(i_uv + off));

						c += Star(f_uv - off - mv) * smoothstep(.5, .51, frac(_Time.y * rand2d(i_uv + off))) * s;
					}
				}
				col.r = c * rand2d(i_uv) *1.;
				col.g = c * rand2d(i_uv)* .7;
				col.b = c* rand2d(i_uv) * .8 ;
				//col.b *= .3 * smoothstep(0., 1., frac(_Time.y * rand2d(i_uv)));
				return col;
			}


			#define PI 3.14159
			fixed4 frag(v2f i) : SV_Target
			{
				

				i.uv -= .5;
				float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
				float3 col = 0;
				//float c = 1. - abs(0.18 * (sin(_Time.y + uv.x * 10) * cos(_Time.y + uv.x * 8)) * sin(_Time.y * 1.37) * cos(_Time.y * uv.x));//
				float fs =.25+ smoothstep(.8,1., _Time.y*6.);
				
				float c =1. - abs(.25*fs*fbm2(uv) * (sin(_Time.y + uv.x * 15.*fs) * cos(_Time.y + uv.x * fbm2(float2(uv.x,1.)))) * sin(_Time.y * 1.37*fs) * cos(_Time.y * uv.x) * 5.*fs);//
				

				float l = sin((uv.y+c) * PI);
				l = smoothstep(0.999, 1., l);
				col.r = l;
				col.g = l * .7;
				col.b = l*.5;
				col += (smoothstep(.05, 1., randomStar(uv)) * 1.);
				//col += randomStar(uv);
				//col = tex2Dproj(_GrabTexture, i.grabUv).rgb;
				col -= 1 - tex2D(_MainTex, i.uv - .5).rgb;
				col += (1 - tex2D(_MainTex, .01+i.uv - .5).rgb) * smoothstep(.2, .5, sin(_Time.y));
				uv *=1.1 + (sin(_Time.y *.39) * cos(_Time.y *.67)) * .4;
				//if (col.r >= 0.000) {
					float2 v = 0;
					float2 w = 0;
					v.x = fbm(uv + sin(_Time.y));
					v.y = fbm(uv + v + cos(_Time.y));
					w.x = fbm(uv + v.x);
					w.y = fbm(uv + w.x);

					float2 z = 0;
					z.x = fbm(uv + w.x);
					z.y = fbm(uv + w.y);
					float f1 = fbm(w + v);
					float f2 = fbm(z + v);
					float scale = 0.024;
					float x1 = f1 * scale;
					float x2 = f2 * scale;
					float y1 = f1 * scale;
					float y2 = f2 * scale;
					col += tex2Dproj(_GrabTexture, i.grabUv).rgb;
					col += tex2Dproj(_GrabTexture, i.grabUv + float4(x1, y1, 0, 0));
					col += tex2Dproj(_GrabTexture, i.grabUv + float4(x2, -y2, 0, 0));
					col += tex2Dproj(_GrabTexture, i.grabUv + float4(-x1, y1, 0, 0));
					col += tex2Dproj(_GrabTexture, i.grabUv + float4(-x2, -y2, 0, 0));
					col /= 5.1;

					
					//col.r = f1 * .06;
					//col.b = f1 * .09;

				//}
				if (col.r < 0.01) col = 0.;

				return fixed4(col, 1);


			}

		ENDCG
		}
		GrabPass{ "_GrabTexture2" }
			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"
				
				#include "../XCGInclude.cginc"
				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 grabUv : TEXCOORD1;
					float4 vertex : SV_POSITION;
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;
				sampler2D _GrabTexture2;
				float _Amount;
				float4 _CPos;
				float4 _XPos;
				float4 _MPos;
				float4 _SLPos;
				float4 _SWPos;
				float _SMin;
				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					o.grabUv = ComputeGrabScreenPos(o.vertex);
					return o;
				}
#define MAX_STEPS 70
#define MAX_DIST 10.
#define SURF_DIST .001
				struct RMHit {
					float d;
					int id;
				};
					
			
				float3 TransformD(float3 p) {
					p.x += sin(_Time.y) * 1.3;
					p.y += sin(_Time.y * .4) * .1;
					p.z -= 3. + 3. * cos(_Time.y);
					p.yz = mul(p.yz, rot2d(sin(_Time.y) * .3));
					p.xy = mul(p.xy, rot2d(cos(_Time.y) * .4));
					p.zx = mul(p.zx, rot2d(3+sin(_Time.y * .48) * .5));

					return p;
				}

				float3 TransformE(float3 p) {
					
					p.x += cos(_Time.y) * 1.3;
					p.y += sin(_Time.y * .7) * .4;
					p.z -= 3. + 3. * sin(_Time.y);
					p.yz = mul(p.yz, rot2d(sin(_Time.y) * .6));
					p.xy = mul(p.xy, rot2d(cos(_Time.y) * .2));
					p.zx = mul(p.zx, rot2d(3+cos(_Time.y * .78) * .5));

					return p;
				}
				
				
				float Dollar(float3 p, float s) {
					float d = dstCappedTorus(p - float3(0, .2 * s, 0), float2(1. * s, .0), .2 * s, .05 * s);
					float3 p2 = p + float3(0, .2 * s, 0);
					p2.xy = mul(p2.xy, rot2d(3.14));
					d = smin(d, dstCappedTorus(p2, float2(1. * s, 0.), .2 * s, .05 * s), .1 * s);
					p2 = p;
					p2.y += 0.2 * s;
					p2.xy = mul(p2.xy, rot2d(.8));
					d = smin(d, dstCappedTorus(p2, float2(.4 * s, 0.3 * s), .2 * s, .05 * s), .001 * s);
					p2 = p;
					p2.y -= 0.2 * s;
					p2.xy = mul(p2.xy, rot2d(4.14));
					d = smin(d, dstCappedTorus(p2, float2(.4 * s, 0.3 * s), .2 * s, .05 * s), .001 * s);
					d = smin(d, dstVerticalCapsule(p + float3(0, .45 * s, 0), .9 * s, .04 * s), .001 * s);
					return d;
				}

				float Euro(float3 p, float s) {
					float3 p2 = p;
					p2.xy = mul(p2.xy, rot2d(-1.5));
					float d = dstCappedTorus(p2, float2(1. * s, .0), .2 * s, .05 * s);
					p2 = p;
					p2.x += -.25 * s;
					p2.y += -.05 * s;
					p2.xy = mul(p2.xy, rot2d(1.57));
					d = smin(d, dstVerticalCapsule(p2, .25 * s, .04 * s), .001 * s);
					p2.x += .1 * s;
					d = smin(d, dstVerticalCapsule(p2, .25 * s, .04 * s), .001 * s);

					return d;

				}
				float dstC(float3 p) {
					p.xz = mul(p.xz, rot2d(PI));
					float3 p2 = p;
					p2.xy = mul(p2.xy, rot2d(-1.56));
					float d = dstCappedTorus(p2, float2(1., .0), .1, .05);
					return d;

				}
				float dstO(float3 p) {
					float3 p2 = p;
					p2.yz = mul(p2.yz, rot2d(-1.56));
					float d = dstTorus(p2, float2(.11, .05));
					return d;
				}

				float dstD(float3 p) {
					p.xz = mul(p.xz, rot2d(PI));
					float d = dstVerticalCapsule(p + float3(-0.02, .1, 0), .2, .05);
					float3 p2 = p - float3(.02, 0, 0);
					p2.xy = mul(p2.xy, rot2d(1.56));
					d = smin(d, dstCappedTorus(p2, float2(1., .0), .1, .05), .001);
					return d;
				}

				float dstE(float3 p) {
					p.xz = mul(p.xz, rot2d(PI));
					float3 p2 = p;
					float d = dstVerticalCapsule(p + float3(-0.01, .1, 0), .18, .05);

					p2.y -= .1;
					p2.xy = mul(p2.xy, rot2d(1.56));
					d = smin(d, dstVerticalCapsule(p2, .15, .05), .01);
					p2 = p;
					p2.x -= 0.01;

					p2.xy = mul(p2.xy, rot2d(1.56));
					d = smin(d, dstVerticalCapsule(p2, .1, .05), .01);
					p2 = +p;
					p2.x -= 0.01;
					p2.y += .1;
					p2.xy = mul(p2.xy, rot2d(1.56));
					d = smin(d, dstVerticalCapsule(p2, .15, .05), .01);
					return d;

				}

				float dstSemiC(float3 p) {
					float d = dstSphere(p - float3(0, .05, 0), .05);
					d = min(d, dstSphere(p + float3(0, .05, 0), .05));
					return d;
				}

				float dstX(float3 p) {
					p.xz = mul(p.xz, rot2d(PI));
					float3 p2 = p;
					p2.y += .1;
					p2.xy = mul(p2.xy, rot2d(.6));
					float d = dstVerticalCapsule(p2, .24, .05);
					p2 = p;
					p2.y += .1;
					p2.x += .27;
					p2.x -= .13;
					p2.xy = mul(p2.xy, rot2d(-.6));
					d = smin(d, dstVerticalCapsule(p2, .24, .05), .01);
					return d;
				}

				float dstR(float3 p) {
					p.xz = mul(p.xz, rot2d(PI));
					float d = dstVerticalCapsule(p + float3(-0.08, .1, 0), .2, .05);
					float3 p2 = p - float3(.04, 0.05, 0);
					p2.xy = mul(p2.xy, rot2d(1.56));
					d = smin(d, dstCappedTorus(p2, float2(1., .0), .05, .05), .001);
					p2 = p;
					p2.x += .025;
					p2.y += .1;
					p2.xy = mul(p2.xy, rot2d(-.6));
					d = smin(d, dstVerticalCapsule(p2, .1, .05), 0.01);
					return d;
				}

				float dstG(float3 p) {
					p.xz = mul(p.xz, rot2d(PI));
					float3 p2 = p;
					p2.xy = mul(p2.xy, rot2d(-1.56));
					float d = dstCappedTorus(p2, float2(1., .0), .1, .05);
					p2 = p;
					p2.y += .1;
					p2.x += .03;
					d = smin(d, dstVerticalCapsule(p2, .05, .05), 0.01);
					p2 = p;
					p2.y += .02;
					p2.xy = mul(p2.xy, rot2d(1.56));
					d = smin(d, dstVerticalCapsule(p2, .03, .05), 0.01);
					p2 = p;
					p2.y -= .1;

					p2.xy = mul(p2.xy, rot2d(1.6));
					d = smin(d, dstVerticalCapsule(p2, .04, .05), 0.01);
					return d;
				}

				float dstM(float3 p) {
					float d = dstVerticalCapsule(p + float3(0, .1, 0), .2, .05);
					float3 p2 = p;
					p2.y += .04;
					p2.x -= .1;
					p2.xy = mul(p2.xy, rot2d(-.6));
					d = smin(d, dstVerticalCapsule(p2, .16, .05), 0.01);
					p2 = p;
					p2.y += .04;
					p2.x -= .1;
					p2.xy = mul(p2.xy, rot2d(.6));
					d = smin(d, dstVerticalCapsule(p2, .16, .05), 0.01);
					d = smin(d, dstVerticalCapsule(p + float3(-.2, .1, 0), .16, .05), 0.01);
					return d;
				}

				float dstU(float3 p) {
					float3 p2 = p;
					p2.y += .02;
					p2.xy = mul(p2.xy, rot2d(PI));
					float d = dstCappedTorus(p2, float2(1., .0), .08, .05);
					d = smin(d, dstVerticalCapsule(p + float3(0.08, .03, 0), .12, .05), 0.01);
					d = smin(d, dstVerticalCapsule(p + float3(-0.08, .03, 0), .12, .05), 0.01);
					return d;
				}
				float dstS(float3 p) {
					p.xz = mul(p.xz, rot2d(PI));
					float3 p2 = p;
					p2.y -= .05;
					p2.xy = mul(p2.xy, rot2d(-1.56));
					float d = dstCappedTorus(p2, float2(1., -.2), .0525, .04);
					p2 = p;
					p2.x -= .03;
					p2.y += .05;
					p2.xy = mul(p2.xy, rot2d(1.56));
					d = smin(d, dstCappedTorus(p2, float2(1., -.2), .0525, .04), 0.02);
					return d;
				}

				float dstI(float3 p) {
					float d = dstVerticalCapsule(p + float3(0, .1, 0), .08, .05);
					d = min(d, dstSphere(p - float3(0, .1, 0), .05));
					return d;
				}

				float dstK(float3 p) {
					p.xz = mul(p.xz, rot2d(PI));
					float d = dstVerticalCapsule(p + float3(0, .1, 0), .2, .05);
					float3 p2 = p;
					p2.x += .12;
					p2.y += .1;
					p2.xy = mul(p2.xy, rot2d(-.8));
					d = smin(d, dstVerticalCapsule(p2, .14, .05), 0.01);
					p2 = p;
					p2.x += .0;
					p2.xy = mul(p2.xy, rot2d(.8));
					d = smin(d, dstVerticalCapsule(p2, .14, .05), 0.01);
					return d;
				}
				float dstN(float3 p) {
					p.xz = mul(p.xz, rot2d(PI));
					float d = dstVerticalCapsule(p + float3(0, .1, 0), .2, .05);
					float3 p2 = p;
					p2.x -= 0.;
					p2.y += .1;
					p2.xy = mul(p2.xy, rot2d(-.6));
					d = smin(d, dstVerticalCapsule(p2, .24, .05), 0.01);
					d = smin(d, dstVerticalCapsule(p - float3(.16, -.1, 0), .2, .05), 0.01);
					return d;
				}
				float dstL(float3 p) {
					float d = dstVerticalCapsule(p + float3(0, .1, 0), .2, .05);
					float3 p2 = p;
					//p2.x += .12 ;
					p2.y += .1;
					p2.xy = mul(p2.xy, rot2d(-1.56));
					d = smin(d, dstVerticalCapsule(p2, .1, .05), 0.01);
					return d;
				}
				float dstV(float3 p) {
					float3 p2 = p;
					p2.y += .1;
					p2.xy = mul(p2.xy, rot2d(-.4));
					float d = dstVerticalCapsule(p2, .23, .05);
					p2 = p;
					p2.y += .1;
					p2.xy = mul(p2.xy, rot2d(.4));
					d = smin(d, dstVerticalCapsule(p2, .23, .05), 0.01);
					return d;

				}

				float dstW(float3 p) {
					float3 p2 = p;
					p2.y += .1;
					p2.xy = mul(p2.xy, rot2d(-.4));
					float d = dstVerticalCapsule(p2, .23, .05);
					p2 = p;
					p2.y += .1;
					p2.xy = mul(p2.xy, rot2d(.4));
					d = smin(d, dstVerticalCapsule(p2, .23, .05), 0.01);
					p2 = p;
					p2.y += .1;
					p2.x -= .2;
					p2.xy = mul(p2.xy, rot2d(-.4));
					d = smin(d, dstVerticalCapsule(p2, .23, .05), 0.01);
					p2 = p;
					p2.y += .1;
					p2.x -= .2;
					p2.xy = mul(p2.xy, rot2d(.4));
					d = smin(d, dstVerticalCapsule(p2, .23, .05), 0.01);
					return d;
				}

				float dstA(float3 p) {
					p.xz = mul(p.xz, rot2d(PI));
					float3 p2 = p;
					p2.y += .12;
					p2.xy = mul(p2.xy, rot2d(.4));
					float d = dstVerticalCapsule(p2, .24, .05);
					p2 = p;
					p2.y += .12;
					p2.x += .2;
					p2.xy = mul(p2.xy, rot2d(-.4));
					d = smin(d, dstVerticalCapsule(p2, .24, .05), 0.01);
					p2 = p;
					p2.y += .06;
					p2.x += .05;
					p2.xy = mul(p2.xy, rot2d(1.56));
					d = smin(d, dstVerticalCapsule(p2, .1, .05), 0.01);

					return d;
				}

				float dstSwallow(float3 p) {
					float d = dstS(p + float3(1., 0, 0));
					d = min(d, dstW(p + float3(.75, 0, 0)));
					d = min(d, dstA(p + float3(.375, 0, 0)));
					d = min(d, dstL(p + float3(.05, 0, 0)));
					d = min(d, dstL(p - float3(.2, 0, 0)));
					d = min(d, dstO(p - float3(.55, 0, 0)));
					d = min(d, dstW(p - float3(.9, 0, 0)));
					return d;
				}
				float dstSLove(float3 p) {
					float d = dstS(p + float3(1.5, 0, 0));
					d = min(d, dstK(p + float3(1.35, 0, 0)));
					d = min(d, dstI(p + float3(1.1, 0, 0)));
					d = min(d, dstN(p + float3(.7825, 0, 0)));
					d = min(d, dstL(p + float3(.65, 0, 0)));
					d = min(d, dstE(p + float3(.4, 0, 0)));
					d = min(d, dstS(p + float3(.075, 0, 0)));
					d = min(d, dstS(p - float3(.1, 0, 0)));
					d = min(d, dstL(p - float3(.4, 0, 0)));
					d = min(d, dstO(p - float3(.75, 0, 0)));
					d = min(d, dstV(p - float3(1.1, 0, 0)));
					d = min(d, dstE(p - float3(1.35, 0, 0)));
					return d;
				}
				float dstMusic(float3 p) {
					float d = dstM(p + float3(.6, 0, 0));
					d = min(d, dstU(p + float3(.175, 0, 0)));
					d = min(d, dstS(p + float3(-.075, 0, 0)));
					d = min(d, dstI(p + float3(-.225, 0, 0)));
					d = min(d, dstC(p + float3(-.45, 0, 0)));
					d = min(d, dstSemiC(p + float3(-.55, 0, 0)));
					return d;
				}

				float dstXreg(float3 p) {
					float d = dstX(p + float3(.45, 0., 0.));
					d = min(d, dstR(p + float3(.1, 0, 0)));
					d = min(d, dstE(p - float3(.075, 0, 0)));
					d = min(d, dstG(p - float3(.45, 0, 0)));
					return d;
				}
				float dstCode(float3 p) {
					float d = dstC(p + float3(.5, 0., 0.));
					d = min(d, dstO(p + float3(.25, 0., 0.)));
					d = min(d, dstD(p - float3(.0, 0., 0.)));
					d = min(d, dstE(p - float3(.25, 0., 0.)));
					d = min(d, dstSemiC(p - float3(.5, 0., 0.)));
					return d;
				}
				float3 TransformCode(float3 p) {

					p += _CPos.xyz;
					p.yz = mul(p.yz, rot2d(sin(_Time.y * .95) * .15));
					p.zx = mul(p.zx, rot2d(sin(_Time.y *1.55)*.45));
					p.xy = mul(p.xy, rot2d(sin(_Time.y * 1.25) * .2));
					//p.yz = mul(p.zx, rot2d(cos(_Time.y * .35)) * .15);
					/*p.yz = mul(p.yz, rot2d(_Time.y * .79));
					p.xy = mul(p.xy, rot2d(_Time.y * .45));
					p.zx = mul(p.zx, rot2d(_Time.y * .35));*/
					return p;
				}
				float3 TransformXreg(float3 p) {
					p += _XPos.xyz;
					p.yz = mul(p.yz, rot2d(cos(_Time.y * .95) * .15));
					p.zx = mul(p.zx, rot2d(cos(_Time.y * 1.55) * .45));
					p.xy = mul(p.xy, rot2d(cos(_Time.y * 1.25) * .2));
					//p.yz = mul(p.zx, rot2d(cos(_Time.y * .75)) * .15);
					/*p.yz = mul(p.yz, rot2d(_Time.y * .49));
					p.xy = mul(p.xy, rot2d(_Time.y * .15));
					p.zx = mul(p.zx, rot2d(_Time.y * .65));*/
					return p;
				}

				float3 TransformMusic(float3 p)
				{
					p += _MPos.xyz;
					p.yz = mul(p.yz, rot2d(sin(_Time.y * 1.5) * .15));
					p.zx = mul(p.zx, rot2d(sin(_Time.y * 1.05) * .35));
					p.xy = mul(p.xy, rot2d(sin(_Time.y * 1.45) * .22));

					return p;
				}

				float3 TransformSLove(float3 p)
				{
					p += _SLPos.xyz;
					p.yz = mul(p.yz, rot2d(cos(_Time.y * 1.5) * .05));
					p.zx = mul(p.zx, rot2d(cos(_Time.y * 1.05) * .1));
					p.xy = mul(p.xy, rot2d(cos(_Time.y * 1.45) * .1));
					
					return p;
				}


				float3 TransformSwallow(float3 p)
				{
					p += _SWPos.xyz;
					p.yz = mul(p.yz, rot2d(cos(_Time.y * 1.5) * .15));
					p.zx = mul(p.zx, rot2d(cos(_Time.y * 1.05) * .25));
					p.xy = mul(p.xy, rot2d(cos(_Time.y * 1.45) * .2));

					return p;
				}
				
				
				RMHit GetDist(float3 p) {
					RMHit hit;

					
					float dd = 0.;
					float de = 0.;
					float num = 4.;
					hit.d = 10.;
					
					for (float i = 0.; i <= 1.; i += 1. / num) {
						float3 p2 = p+float3(sin(i*4.+_Time.y)*.3, cos(i*4.+_Time.y)*.2, 0.);
						float3 p3 = p - float3(sin(i * 4. + _Time.y) * .3, cos(i * 4. + _Time.y) * .2, 0.);
						dd = Dollar(TransformD(p2), .8);
						de = Euro(TransformE(p3),1.);
						//float dBlend = lerp(dd, de, blendF);
						float d = smin(dd, de, .1);
						hit.d = smin(hit.d, d, .1);
						
					}
					float code = dstCode(TransformCode(p));
					float xreg = dstXreg(TransformXreg(p));
					float music = dstMusic(TransformMusic(p));
					float slove = dstSLove(TransformSLove(p));
					float swallow = dstSwallow(TransformSwallow(p));
					hit.d = smin(hit.d, code, .1);
					hit.d = smin(hit.d, xreg, _SMin);
					hit.d = smin(hit.d, music, _SMin);
					hit.d = smin(hit.d, slove, _SMin);
					hit.d = smin(hit.d, swallow, _SMin);
					
					hit.id = 2;
					//if (dd == hit.d) hit.id = 1;
					//if (de == hit.d) hit.id = 2;

					return hit;
				}

				RMHit RayMarch(float3 ro, float3 rd) {
					float dO = 0.;
					RMHit hit;
					for (int i = 0; i < MAX_STEPS; i++) {
						float3 p = ro + rd * dO;
						hit = GetDist(p);

						dO += hit.d;
						if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;

					}
					hit.d = dO;
					return hit;
				}

				float3 GetNormal(float3 p) {
					float d = GetDist(p).d;

					float2 e = float2(0.01, 0);

					float3 n = d - float3(
						GetDist(p - e.xyy).d,
						GetDist(p - e.yxy).d,
						GetDist(p - e.yyx).d);
					return normalize(n);

				}

				float GetLight(float3 p, float3 lightPos) {

					float3 l = normalize(lightPos - p);
					float3 n = GetNormal(p);
					float dif = clamp(dot(n, l), 0., 1.);
					float d = RayMarch(p + n * SURF_DIST * 3., l).d;
					if (d < length(lightPos - p)) dif *= .1; // shadow
					return dif;
				}

				float3 GetRayDir(float2 uv, float3 p, float3 l, float z) {
					float3 f = normalize(l - p),
						r = normalize(cross(float3(0, 1, 0), f)),
						u = cross(f, r),
						c = f * z,
						i = c + uv.x * r + uv.y * u,
						d = normalize(i);
					return d;
				}


				fixed4 frag(v2f i) : SV_Target
				{
					i.uv -= .5;
					float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
					float3 col = 0;
					col = tex2Dproj(_GrabTexture2, i.grabUv).rgb;
					float3 ro = float3(0, 0, -1);

					float3 rd = GetRayDir(uv, ro, 0, 1.);

					RMHit hit = RayMarch(ro, rd);
					
					if (hit.d < MAX_DIST) {
						float3 p = ro + rd * hit.d;
						if (hit.id < 3) {
							float3 n = GetNormal(p);
							float3 r = reflect(rd, n);
							float spec = pow(max(0., r.y), 30.);

							float dif = dot(n, normalize(float3(1, 2, 3))) * .5 + .5;
							float l = GetLight(p, float3(sin(_Time.y * .1) * 1., 0, -2));
							i.grabUv.xyz = r.xyz;
							float3 bgr = tex2Dproj(_GrabTexture2, i.grabUv).rgb;
							col = lerp(bgr, l, .4) + spec;

						}


					}
					col = lerp(0., col, _Amount);
					//if (_Amount != 1) col.g = .4;
					return fixed4(col, 1);
				}
				ENDCG

			}
	

	}
}