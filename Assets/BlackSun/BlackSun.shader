﻿Shader "BlackSun"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Amount("Amount",float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        
        GrabPass { "_GrabTexture" }
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
           
            #include "UnityCG.cginc"
            #include "../XCGInclude.cginc"
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 grabUv : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _GrabTexture;
            float _Amount;
            #define PART_TWO  16
            #define MAX_STEPS 50
            #define MAX_DIST 10.
            #define SURF_DIST .001
            struct RMHit {
                float d;
                int id;
                float l;
            };
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.grabUv = ComputeGrabScreenPos(o.vertex);
                
                return o;
            }

            float sunLine(float2 p) {
                float c = 0.;
                float ls = .025;
                c += step(.1, smoothstep(ls, -ls, p.y) * smoothstep(-ls, ls, p.y)
                    * step(0.1, smoothstep(.5, -.4, p.x)) * step(0.1, smoothstep(-.4, .0, p.x)));
                c += step(0.1, smoothstep(.1, 0.07, p.y) * smoothstep(-.001, 0., p.y)
                    * smoothstep(-.331, -.29, p.x) * smoothstep(-.29, -.30, p.x));
                float lso = .09;
                c += step(.1, smoothstep(ls + lso, -ls + lso, p.y) * smoothstep(-ls + lso, ls + lso, p.y)
                    * step(0.1, smoothstep(-.29, -.3, p.x)) * step(0.1, smoothstep(-.53, .0, p.x)));
                lso = -.09;
                float lsox = .617;
                c += step(0.1, smoothstep(.1 + lso, 0.07 + lso, p.y) * smoothstep(-.001 + lso, 0. + lso, p.y)
                    * smoothstep(-.331 + lsox, -.29 + lsox, p.x) * smoothstep(-.29 + lsox, -.30 + lsox, p.x));

                lso = -.09;
                lsox = .72;
                c += step(.1, smoothstep(ls + lso, -ls + lso, p.y) * smoothstep(-ls + lso, ls + lso, p.y)
                    * step(0.1, smoothstep(-.29 + lsox, -.3 + lsox, p.x)) * step(0.1, smoothstep(-.53 + lsox, .0 + lsox, p.x)));

                return c;
            }
            float blackSun(float2 p) {

                float d = length(p);
                float c = step(.5, smoothstep(0.14, 0.07, d));
                c += step(.0010, smoothstep(.17, .0, d) * smoothstep(.14, .2, d));
                c += step(.00010, smoothstep(.45, .0, d) * smoothstep(.412, .5, d));

                for (float a = 0.; a <= PI2; a += PI2 / 12.) {
                    c += sunLine(mul(p, rot2d(a)));
                }

                return c;
            }
            float fbm(float2 p) {

                float freq = 1.3 + 1.2 * sin(_Time.y * .71);
                float amp = 1.0;
                float v = 0.;
                for (int i = 0; i < 6; i++) {
                    float x = float(i); // sin(_Time.y * 4);//float(i);
                    float y = cos(_Time.y * 3) * sin(_Time.y * 3.79);
                    //mat2 R = mat2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    float2x2 R = float2x2(cos(.5), -sin(.5), sin(.5), cos(.5));
                    p.xy *= mul(p, R);
                    v += amp * noise2d(x + y + p * freq);
                    freq *= 17.5 + 10.5 * sin(_Time.y * 1.37); // 2
                    amp *= .5; // .5
                }
                return v;
            }
            float fbm2(float2 p) {

                float freq = 1.;
                float amp = 1.;
                float v = 0.;
                for (int i = 0; i < 6; i++) {
                    float x = float(i);
                    float y = cos(i * 2.);
                    float2x2 R = float2x2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    v += amp * noise2d(x + y + mul(p, R) * freq);
                    freq *= 2.;
                    amp *= .5;
                }
                return v;
            }
            float skull_talk(float3 p, float ta) {
                float d = dstEllipsoid(p, float3(.2, .2, .2));
                float3 p2 = p - float3(.0, -0.18 - ta, -0.02);
                p2.yz = mul(p2.yz, rot2d(1.4));
                float chin = dstEllipsoid(p2, float3(.13 - ta * .5, .18, .14));
                d = smin(d, chin, .1);
                float srem = dstEllipsoid(p - float3(0, -0.3, .2), float3(.4, .25, .3));
                d = smax(d, -srem, .1);
                float front = dstEllipsoid(p - float3(0, 0.15, 0), float3(.16, .1, .2));
                d = smin(front, d, .1);

                float hole = dstEllipsoid(p - float3(0.02, 0.1, 0.1), float3(.1, .1, .1));
                d = smax(d, -hole, .2);
                float lchin = dstEllipsoid(p - float3(0.2, 0.2, 0.), float3(.1, .1, .2));
                d = smax(-lchin, d, .1);


                float rchin = dstEllipsoid(p - float3(-0.2, -0.2, 0.), float3(.1, .1, .2));
                d = smax(-rchin, d, .1);

                float rc = dstEllipsoid(p - float3(0.1, -0.05, -0.03), float3(.1, .1, .1));
                d = smax(-rc, d, .1);


                float lc = dstEllipsoid(p - float3(-0.1, -0.05, -0.05), float3(.1, .1, .1));
                d = smax(-lc, d, .1);


                float eyel = dstEllipsoid(p + float3(-.06, -0.05, .1), float3(.05, .045, .1));
                d = smax(d, -eyel, .1);
                float eyer = dstEllipsoid(p + float3(+.06, -0.05, .1), float3(.05, .045, .1));
                d = smax(d, -eyer, .1);

                p2 = p + float3(0.014, .04, .2);
                p2.xy = mul(p2.xy, rot2d(.6));
                float nosel = dstEllipsoid(p2, float3(.015, .03, .2));
                d = smax(d, -nosel, .1);
                p2 = p + float3(-0.014, 0.04, .2);
                p2.xy = mul(p2.xy, rot2d(-.6));
                float noser = dstEllipsoid(p2, float3(.015, .03, .2));
                d = smax(d, -noser, .1);

                float cmouth2 = dstEllipsoid(p + float3(0.002, .17 + ta, 0.1), float3(.092, .05 + ta, .12));
                d = smax(d, -cmouth2, .001);

                float t = dstRoundBox(p - float3(-.05, -.165, -0.182), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.001);
                t = dstRoundBox(p - float3(-.03, -.165, -0.19), float3(.006, .01, .004), .001);
                d = smin(d, t, 0.001);
                t = dstRoundBox(p - float3(-.01, -.16, -0.195), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.005);
                t = dstRoundBox(p - float3(.01, -.16, -0.19), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.005);
                t = dstRoundBox(p - float3(.03, -.16, -0.19), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.005);
                t = dstRoundBox(p - float3(.05, -.15, -0.19), float3(.008, .01, .004), .003);
                d = smin(d, t, 0.007);

                t = dstRoundBox(p - float3(.05, -.188 - ta, -0.19), float3(.005, .01, .004), .005);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(.03, -.19 - ta, -0.2), float3(.005, .01, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(.01, -.194 - ta, -0.2), float3(.004, .01, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(-.01, -.192 - ta, -0.21), float3(.004, .009, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(-.035, -.19 - ta, -0.2), float3(.004, .005, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(-.055, -.19 - ta, -0.18), float3(.002, .003, .004), .003);
                d = smin(d, t, 0.01);

                float brain = dstEllipsoid(p - float3(0, .1, 0), float3(.10, .10, .10));
                float rr = 0.;
                /*if ( (_Time.y - PART_TWO) > 0 ) rr = (_Time.y - PART_TWO) * .003;*/
                float f = (fbm2(p.xz * 10.)) * (0.01+rr*.25);
                d -= f;
                f = (fbm2(p.xz * 7.)) * (0.007+rr);
                d += f;
                return d;

            }

            float Dollar(float3 p, float s) {
                float d = dstCappedTorus(p - float3(0, .2 * s, 0), float2(1. * s, .0), .2 * s, .05 * s);
                float3 p2 = p + float3(0, .2 * s, 0);
                p2.xy = mul(p2.xy, rot2d(3.14));
                d = smin(d, dstCappedTorus(p2, float2(1. * s, 0.), .2 * s, .05 * s), .1 * s);
                p2 = p;
                p2.y += 0.2 * s;
                p2.xy = mul(p2.xy, rot2d(.8));
                d = smin(d, dstCappedTorus(p2, float2(.4 * s, 0.3 * s), .2 * s, .05 * s), .001 * s);
                p2 = p;
                p2.y -= 0.2 * s;
                p2.xy = mul(p2.xy, rot2d(4.14));
                d = smin(d, dstCappedTorus(p2, float2(.4 * s, 0.3 * s), .2 * s, .05 * s), .001 * s);
                d = smin(d, dstVerticalCapsule(p + float3(0, .45 * s, 0), .9 * s, .04 * s), .001 * s);
                return d;
            }
            float3 TransformD(float3 p) {
                p.x += sin(_Time.y * .8) * 1.;
                p.y += sin(_Time.y * .3) * .1;
                p.z -= 2. + 2.1 * cos(_Time.y *.8);
                p.yz = mul(p.yz, rot2d(sin(_Time.y) * .3));
                p.xy = mul(p.xy, rot2d(cos(_Time.y) * .4));
                p.zx = mul(p.zx, rot2d(3 + sin(_Time.y * .48) * .5));

                return p;
            }

            float3 TransformW(float3 p) {
                p.x += sin(_Time.y * .8) * 1.;
                p.y += sin(_Time.y * .3) * .1;
                p.z -= 2. + 2.1 * cos(_Time.y * .8);
                p.yz = mul(p.yz, rot2d(sin(_Time.y) * .3));
                p.xy = mul(p.xy, rot2d(cos(_Time.y) * .4));
                p.zx = mul(p.zx, rot2d(sin(_Time.y * .48) * .5));

                return p;
            }
            float3 TransformS(float3 p) {
                p.z += .1 + sin(_Time.y * 2.1) * .1;
                //p.xyz += _SPos.xyz;
                //p.xyz += _SPos.xyz;
                p.xy = mul(p.xy, rot2d(cos(_Time.y * 1.9) * .05));
                p.zx = mul(p.xz, rot2d(1.56));
                
                return p;

            }
            float dst_w(float3 p, float s) {
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(1.2));
                float d = dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s);
                p2 = p;
                p2 -= float3(.2 * s, 0., 0.);
                p2.xy = mul(p2.xy, rot2d(1.2));
                d = smin(d, dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                p2 = p - float3(.1 * s, 0., 0.);
                p2.xy = mul(p2.xy, rot2d(-1.2));
                d = smin(d, dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                p2 = p;
                p2 -= float3(.3 * s, 0., 0.);
                p2.xy =mul(p2.xy, rot2d(-1.2));
                d = smin(d, dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                return d;
            }

            float dst_a(float3 p, float s) {
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(-1.2));
                float d = dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s);
                p2 = p - float3(.12 * s, 0., 0.);
                p2.xy = mul(p2.xy, rot2d(1.2));
                d = smin(d, dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                d = smin(d, dstRoundBox(p + float3(-0.06, 0.025 * s, 0.), float3(.06 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                return d;
            }

            float dst_r(float3 p, float s) {
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(-1.56));
                float d = dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s);

                d = smin(d, dstRoundBox(p + float3(-.08 * s, -.158 * s, 0), float3(.05 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                d = smin(d, dstRoundBox(p + float3(-.08 * s, -.01 * s, 0), float3(.05 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                p2 = p + float3(-.15 * s, -.08 * s, 0);
                p2.xy = mul(p2.xy, rot2d(-1.56));
                d = smin(d, dstRoundBox(p2, float3(.07 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                p2 = p + float3(-.12 * s, .08 * s, 0);
                p2.xy = mul(p2.xy,rot2d(-1.2));
                d = smin(d, dstRoundBox(p2, float3(.095 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);

                return d;
            }
            float war(float3 p, float s) {
                float3 p2 = p + float3(.5 * s, 0., 0);
                p2.yz = mul(p2.yz, rot2d(3.14159));
                float d = dst_w(p2, s);
                p2 = p;
                p2.yz = mul(p2.yz, rot2d(3.14159));
                d = smin(d, dst_a(p2, s), .01 * s);
                d = smin(d, dst_r(p - float3(.35 * s, 0., 0.), s), .01 * s);

                return d;

            }

            RMHit GetDist(float3 p) {
                RMHit hit;
                float ta = 0;
                if ((_Time.y > 2.25) && (_Time.y<PART_TWO)) ta = abs(sin(_Time.y * 6.)) * .013;
                //hit.d = xkikkel7(TransformX(p), .7);
                //hit.d = smin(hit.d, dstMan(TransformMan(p), .16), .1);
                //hit.d = smin(hit.d, skull_talk(TransformS(p), ta), .1);
                //hit.d = dstShip(TransformShip(p), .16);
                
                float ds = skull_talk(TransformS(p), ta);
                float dd = 0.;
                
                dd = Dollar(TransformD(p), 1.);
             
                //dd = war(TransformW(p), 1.);
                hit.d = smin(ds, dd,.125); //, .125);
                float dif1 = 0.;
                if (hit.d <= SURF_DIST) {
                    dif1 = 1. - (SURF_DIST - ds) + (SURF_DIST - dd); //NICE RESULT
                    //dif1 = 1. - md-ad;
                    
                    if ((SURF_DIST - dd) >= (SURF_DIST - ds)) {
                        
                        // close to akka
                        dif1 = 1.;//- ((SURF_DIST-md) + (SURF_DIST-ad));

                    }
                    hit.id = 3;

                }
                if ((hit.d <= dd + SURF_DIST) && (hit.d >= dd - SURF_DIST)) {
                    hit.id = 1;

                }
                /*if ((hit.d <= ds + SURF_DIST) && (hit.d >= ds - SURF_DIST)) {
                    hit.id = 2;

                }*/
                
                if (ds == hit.d) hit.id = 2;

                hit.l = dif1;
                
                //hit.id = 1;
                return hit;
            }

            RMHit RayMarch(float3 ro, float3 rd) {
                float dO = 0.;
                RMHit hit;
                hit.d = 0;
                for (int i = 0; i < MAX_STEPS; i++) {
                    float3 p = ro + rd * dO;
                    hit = GetDist(p);

                    dO += hit.d;
                    if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;

                }
                hit.d = dO;
                return hit;
            }

            float3 GetNormal(float3 p) {
                float d = GetDist(p).d;

                float2 e = float2(0.01, 0);
                float3 n = d - float3(
                    GetDist(p - e.xyy).d,
                    GetDist(p - e.yxy).d,
                    GetDist(p - e.yyx).d);
                return normalize(n);

            }


            float GetLight(float3 p, float3 lightPos) {

                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p);
                float dif = clamp(dot(n, l), 0., 1.);
                float d = RayMarch(p + n * SURF_DIST * 3., l).d;
                //if (d < length(lightPos - p)) dif *= .1; // shadow
                return dif;
            }

            float3 GetRayDir(float2 uv, float3 p, float3 l, float z) {
                float3 f = normalize(l - p),
                    r = normalize(cross(float3(0, 1, 0), f)),
                    u = cross(f, r),
                    c = f * z,
                    i = c + uv.x * r + uv.y * u,
                    d = normalize(i);
                return d;
            }
            float pLine(float2 uv, float s) {
                float c = 0.;
                c = smoothstep(-.001 * s, .0, uv.y);
                c *= smoothstep(.001 * s, .0, uv.y);
                c *= smoothstep(-.5 * s, -.2 * s, uv.x);
                c *= smoothstep(.5 * s, .2 * s, uv.x);
                return c;
            }
            float pStar(float2 uv) {
                float num_s = 128;
                float c = 0.;
                for (float a = 0.; a < 1.; a += 1. / num_s) {
                    float2 uv2 = mul(uv, rot2d(a * 3.14159));
                    float s = .8 + .3 * step(0.5, abs(sin(a * 1.55 * num_s)));
                    c += pLine(uv2, s);
                }
                return c;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
                float2 uvR = uv;
                float3 col = 0;
                //if (sign(sin(_Time.y)) < 0) {
                

                uv = mul(uv, rot2d(cos(_Time.y) * 2.1));
                uv *= 1. + .5 * sin(_Time.y);
                uv += sin(_Time.y) * .5;
                float2 p = frac(uv) - .5;
                //float c = step(.01,blackSun(mul(p, rot2d(_Time.y))));
                float c = 0;
                float max_l = 10;
                for (float l = 1; l < max_l; l += 2.) {
                    //c += step(.1, blackSun(frac(p * l + l) - .5) *(1. - (1. / (max_l / l))));
                    c += step(.01, blackSun(mul(frac(p * l + l * .37) - .5, rot2d(_Time.y))));
                }
                col = c;

                if (col.r >= 0.0) {

                    float x1 = -0.2 * abs(sin((_Time.y + uv.y) * (sin(uv.x * 4. * sin(_Time.y + uv.y)) * 7.)));
                    float x2 = -0.2 * abs(sin((_Time.y + uv.y) * (sin(uv.x * 6. * sin(_Time.y + uv.y)) * 8.)));
                    float y1 = -0.1 * abs(sin((_Time.y + uv.x) * (sin(uv.y * 14. * sin(_Time.y + uv.x * 1.7)) * 5.)));
                    float y2 = -0.1 * abs(sin((_Time.y + uv.x) * (sin(uv.y * 16. * sin(_Time.y + uv.x * 5.7)) * 6.)));
                    col += tex2Dproj(_GrabTexture, i.grabUv).rgb;
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(x1, y1, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(x2, y2, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(-x1, y1, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(-x2, y2, 0, 0));
                    col.r /= 5.3;
                    col.g /= 6.4;
                    col.b /= 7;
                }
                if (col.r >= 0.0) {
                    float x1 = .02;
                    float x2 = .02;
                    float y1 = -.02;
                    float y2 = -.02;
                    col += tex2Dproj(_GrabTexture, i.grabUv).rgb;
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(x1, y1, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(x2, -y2, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(-x1, y1, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(-x2, -y2, 0, 0));
                    col /= 6.3;

                }

                float3 ro = float3(0., 0., -1.);

                float3 rd = GetRayDir(uvR, ro, 0, 1.);
                rd = normalize(float3(uvR.x, uvR.y, 1.));
                RMHit hit = RayMarch(ro, rd);
                //col += Bgr(uv);
                if (hit.d < MAX_DIST) {
                    float3 p = ro + rd * hit.d;
                    float3 n = GetNormal(p);
                    float3 r = reflect(rd, n);
                    float spec = pow(max(0., r.y), 10.);
                    float l = GetLight(p, float3(0, 0, -3));
                    float3 dmat = float3(1, .9, .1);
                    float3 smat = float3(1., 1., 1.);
                    float3 wmat = float3(.2, .2, .2);
                    if (hit.id == 1) {
                        //float dif = dot(n, normalize(float3(1, 3.2, .3))) * .5 + .5;
                        
                        col = lerp(dmat, l, .4) + spec;
                    
                        
                    }
                    else if (hit.id == 2) {
                        
                            col = lerp(smat, l, .4) + spec;
                        
                    }
                    else if (hit.id == 3) {
                        
                        col = lerp(lerp(smat, dmat, hit.l), l, .4) + spec;
                        
                    }
                    
                    
                    

                }
                col = lerp(0., col, _Amount);
                return fixed4(col, 1);
            }
            ENDCG
        }
    }
}
