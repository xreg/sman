﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BlackSunMainCamera :SceneCameraBase
{
    
    public GameObject bgr;
    int fadeState = 0;
    Material mat = null;
    //private float timer = 0;
    public float amount = 0;
    void Start()
    {
        base.Start();
        mat = bgr.GetComponent<Renderer>().material;
        mat.SetFloat("_Amount", 0f);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (fadeState == 0)
        {

            if (timer > 0)
            {
                fadeState++;
            }
        }
        if (fadeState == 1)
        {
            amount += Time.deltaTime / 2f;
            if (amount >= 1.0f) amount = 1.0f;
            mat.SetFloat("_Amount", amount);
            if (timer > 4)
            {
                amount = 1.0f;
                mat.SetFloat("_Amount", amount);
                fadeState++;
            }
        }
        if ((fadeState == 2) && (timer > (SceneTimes.BlackSunScene - 0.5f)))
        {
            fadeState++;
        }
        if (timer > SceneTimes.BlackSunScene)
        {
            SceneManager.LoadScene("WarScene");
        }
        if (fadeState == 3)
        {
            amount -= Time.deltaTime *2;

            mat.SetFloat("_Amount", amount);
        }
    }
}

