﻿Shader "SecondShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _MPos("ManPos", Vector) = (1, 0, 1, 0)
        _SPos("SkullPos", Vector) = (1, 0, 1, 0)
        _Amount("Amount",float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"            
            #include "../XCGInclude.cginc"
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;                
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;                
            };

            sampler2D _MainTex;
            sampler2D _MainTex2;
            float4 _MainTex_ST;
            float4 _SPos;
            float4 _MPos;
            float _Amount;
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
               
                return o;
            }
            #define MAX_STEPS 60
            #define MAX_DIST 10.
            #define SURF_DIST .001
            #define FADE_OUT 15
            struct RMHit {
                float d;
                int id;
            };
            float2 rand(float2 p) {
                float2 r = frac(sin(float2(dot(p, float2(322.76, 705.32)), dot(p, float2(2599.13, 965.67)))) * 5674.249);
                return r;

            }

            float fbm(float2 p) {

                float freq = 1. + 1. * sin(_Time.y * .23);
                float amp = 1. + 1. * cos(_Time.y * .29);
                float v = 0.;
                for (int i = 0; i < 6; i++) {
                    float x = float(i);
                    float y = cos(i * 2.);
                    float2x2 R = float2x2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    v += amp * noise2d(x+y+mul(p, R) * freq);
                    freq *= 2.;
                    amp *= .5;
                }
                return v;
            }

            float fbm2(float2 p) {

                float freq = 1.;
                float amp = 1.;
                float v = 0.;
                for (int i = 0; i < 6; i++) {
                    float x = float(i);
                    float y = cos(i * 2.);
                    float2x2 R = float2x2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    v += amp * noise2d(x+y+mul(p, R) * freq);
                    freq *= 2.;
                    amp *= .5;
                }
                return v;
            }
            float3 Bgr(float2 uv) {
                fixed3 col = 0;
              
                uv = mul(uv, rot2d(sin(_Time.y * .2) * cos(_Time.y * .46)));
                uv *= 4.5 + 3. * sin(_Time.y * .56) * cos(_Time.y * .72);

                float2 i_uv = floor(uv);
                float2 f_uv = frac(uv);
                float2 v = 0;
                float2 w = 0;

                float md = 1.;
                float d = 0.;
                float ys = 1.;
                float xs = 1.;
                float f = 0.;
                for (float y = -ys; y <= ys; y++) {
                    for (float x = -xs; x <= xs; x++) {

                        float2 nb = float2(x, y);
                        v.x = fbm(i_uv + f_uv + sin(_Time.y * .67));

                        v.y = fbm(i_uv + f_uv + v.x + cos(_Time.y * .36));
                        w.x = fbm(i_uv + f_uv + v.y + cos(_Time.y * 1.2));
                        w.y = fbm(i_uv + f_uv + w.x + sin(_Time.y * .13));
                        f = fbm(w + v);
                       
                        float2 p = rand(i_uv + nb) + rand2d(i_uv + nb);
                        p = rand2d(i_uv + nb) + rand2d(i_uv + nb) * sin(_Time.y + PI2 * p * rand2d(i_uv + nb));
                        float2 diff = nb + (p * .5) - f_uv - distance(w, v) * sin(sin(_Time.y * .2) * .56); //clamp(f*f*f*f*f * sin(_Time.y*.13), 0., 52.);

                        d = length(diff);

                        md = min(md, d);

                    }

                }


                col = lerp(col, float3(w.x, f, md + v.y), md);
                col *= lerp(col, float3(f, md, w.y), md) * .9;
                col += md * .2;
                
                return col;

            }

            float skull_talk(float3 p, float ta) {
                float d = dstEllipsoid(p, float3(.2, .2, .2));
                float3 p2 = p - float3(.0, -0.18 - ta, -0.02);
                p2.yz = mul(p2.yz,rot2d(1.4));
                float chin = dstEllipsoid(p2, float3(.13 - ta * .5, .18, .14));
                d = smin(d, chin, .1);
                float srem = dstEllipsoid(p - float3(0, -0.3, .2), float3(.4, .25, .3));
                d = smax(d, -srem, .1);
                float front = dstEllipsoid(p - float3(0, 0.15, 0), float3(.16, .1, .2));
                d = smin(front, d, .1);

                float hole = dstEllipsoid(p - float3(0.02, 0.1, 0.1), float3(.1, .1, .1));
                d = smax(d, -hole, .2);
                float lchin = dstEllipsoid(p - float3(0.2, 0.2, 0.), float3(.1, .1, .2));
                d = smax(-lchin, d, .1);


                float rchin = dstEllipsoid(p - float3(-0.2, -0.2, 0.), float3(.1, .1, .2));
                d = smax(-rchin, d, .1);

                float rc = dstEllipsoid(p - float3(0.1, -0.05, -0.03), float3(.1, .1, .1));
                d = smax(-rc, d, .1);


                float lc = dstEllipsoid(p - float3(-0.1, -0.05, -0.05), float3(.1, .1, .1));
                d = smax(-lc, d, .1);


                float eyel = dstEllipsoid(p + float3(-.06, -0.05, .1), float3(.05, .045, .1));
                d = smax(d, -eyel, .1);
                float eyer = dstEllipsoid(p + float3(+.06, -0.05, .1), float3(.05, .045, .1));
                d = smax(d, -eyer, .1);

                p2 = p + float3(0.014, .04, .2);
                p2.xy = mul(p2.xy, rot2d(.6));
                float nosel = dstEllipsoid(p2, float3(.015, .03, .2));
                d = smax(d, -nosel, .1);
                p2 = p + float3(-0.014, 0.04, .2);
                p2.xy = mul(p2.xy,rot2d(-.6));
                float noser = dstEllipsoid(p2, float3(.015, .03, .2));
                d = smax(d, -noser, .1);

                float cmouth2 = dstEllipsoid(p + float3(0.002, .17 + ta, 0.1), float3(.092, .05 + ta, .12));
                d = smax(d, -cmouth2, .001);

                float t = dstRoundBox(p - float3(-.05, -.165, -0.182), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.001);
                t = dstRoundBox(p - float3(-.03, -.165, -0.19), float3(.006, .01, .004), .001);
                d = smin(d, t, 0.001);
                t = dstRoundBox(p - float3(-.01, -.16, -0.195), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.005);
                t = dstRoundBox(p - float3(.01, -.16, -0.19), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.005);
                t = dstRoundBox(p - float3(.03, -.16, -0.19), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.005);
                t = dstRoundBox(p - float3(.05, -.15, -0.19), float3(.008, .01, .004), .003);
                d = smin(d, t, 0.007);

                t = dstRoundBox(p - float3(.05, -.188 - ta, -0.19), float3(.005, .01, .004), .005);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(.03, -.19 - ta, -0.2), float3(.005, .01, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(.01, -.194 - ta, -0.2), float3(.004, .01, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(-.01, -.192 - ta, -0.21), float3(.004, .009, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(-.035, -.19 - ta, -0.2), float3(.004, .005, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(-.055, -.19 - ta, -0.18), float3(.002, .003, .004), .003);
                d = smin(d, t, 0.01);

                float brain = dstEllipsoid(p - float3(0, .1, 0), float3(.10, .10, .10));
                
                float f = (fbm2(p.xz * 10.)) * 0.01;
                d -= f;
                f = (fbm2(p.xz * 7.)) * 0.007;
                d += f;
                return d;

            }
            float Skull(float3 p) {
                float d = dstEllipsoid(p, float3(.2, .2, .2));
                float3 p2 = p - float3(.0, -0.18, -0.02);
                p2.yz = mul(p2.yz, rot2d(1.4));
                float chin = dstEllipsoid(p2, float3(.13, .18, .14));
                d = smin(d, chin, .1);
                float srem = dstEllipsoid(p - float3(0, -0.3, .2), float3(.4, .25, .3));
                d = smax(d, -srem, .1);
                float front = dstEllipsoid(p - float3(0, 0.15, 0), float3(.16, .1, .2));
                d = smin(front, d, .1);

                float hole = dstEllipsoid(p - float3(0.02, 0.1, 0.1), float3(.1, .1, .1));
                d = smax(d, -hole, .2);
                float lchin = dstEllipsoid(p - float3(0.2, 0.2, 0.), float3(.1, .1, .2));
                d = smax(-lchin, d, .1);


                float rchin = dstEllipsoid(p - float3(-0.2, -0.2, 0.), float3(.1, .1, .2));
                d = smax(-rchin, d, .1);

                float rc = dstEllipsoid(p - float3(0.1, -0.05, -0.03), float3(.1, .1, .1));
                d = smax(-rc, d, .1);


                float lc = dstEllipsoid(p - float3(-0.1, -0.05, -0.05), float3(.1, .1, .1));
                d = smax(-lc, d, .1);

                float eyel = dstEllipsoid(p + float3(-.06, -0.05, .1), float3(.05, .045, .1));
                d = smax(d, -eyel, .1);
                float eyer = dstEllipsoid(p + float3(+.06, -0.05, .1), float3(.05, .045, .1));
                d = smax(d, -eyer, .1);

                p2 = p + float3(0.014, .04, .2);
                p2.xy = mul(p2.xy,rot2d(.6));
                float nosel = dstEllipsoid(p2, float3(.015, .03, .2));
                d = smax(d, -nosel, .1);
                p2 = p + float3(-0.014, 0.04, .2);
                p2.xy = mul(p2.xy, rot2d(-.6));
                float noser = dstEllipsoid(p2, float3(.015, .03, .2));
                d = smax(d, -noser, .1);

                float t = dstRoundBox(p - float3(-.05, -.13, -0.2), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);
                t = dstRoundBox(p - float3(-.03, -.15, -0.15), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);
                t = dstRoundBox(p - float3(-.01, -.15, -0.1), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);
                t = dstRoundBox(p - float3(.01, -.15, -0.1), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);
                t = dstRoundBox(p - float3(.03, -.13, -0.15), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);
                t = dstRoundBox(p - float3(.05, -.15, -0.2), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);

                t = dstRoundBox(p - float3(-.05, -.19, -0.2), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);
                t = dstRoundBox(p - float3(-.03, -.19, -0.15), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);
                t = dstRoundBox(p - float3(-.01, -.19, -0.1), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);
                t = dstRoundBox(p - float3(.01, -.19, -0.1), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);
                t = dstRoundBox(p - float3(.03, -.19, -0.15), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);
                t = dstRoundBox(p - float3(.05, -.19, -0.2), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);

                float brain = dstEllipsoid(p - float3(0, .1, 0), float3(.10, .10, .10));
                d = smax(d, -brain, .1);
                //d -= fbm(p.xz) * .01;

                float f = (fbm2(p.xz * 10.)) * 0.013;
                d -= f;
                f = (fbm2(p.xz * 7.)) * 0.01;
                d += f;
                return d;

            }
            
            float Dollar(float3 p, float s) {
                float d = dstCappedTorus(p - float3(0, .2 * s, 0), float2(1. * s, .0), .2 * s, .05 * s);
                float3 p2 = p + float3(0, .2 * s, 0);
                p2.xy = mul(p2.xy,rot2d(3.14));
                d = smin(d, dstCappedTorus(p2, float2(1. * s, 0.), .2 * s, .05 * s), .1 * s);
                p2 = p;
                p2.y += 0.2 * s;
                p2.xy = mul(p2.xy,rot2d(.8));
                d = smin(d, dstCappedTorus(p2, float2(.4 * s, 0.3 * s), .2 * s, .05 * s), .001 * s);
                p2 = p;
                p2.y -= 0.2 * s;
                p2.xy = mul(p2.xy,rot2d(4.14));
                d = smin(d, dstCappedTorus(p2, float2(.4 * s, 0.3 * s), .2 * s, .05 * s), .001 * s);
                d = smin(d, dstVerticalCapsule(p + float3(0, .45 * s, 0), .9 * s, .04 * s), .001 * s);
                return d;
            }

            float Euro(float3 p, float s) {
                float3 p2 = p;
                p2.xy = mul(p2.xy,rot2d(-1.5));
                float d = dstCappedTorus(p2, float2(1. * s, .0), .2 * s, .05 * s);
                p2 = p;
                p2.x += .25 * s;
                p2.y += .05 * s;
                p2.xy = mul(p2.xy, rot2d(1.57));
                d = smin(d, dstVerticalCapsule(p2, .25 * s, .04 * s), .001 * s);
                p2.x += .1 * s;
                d = smin(d, dstVerticalCapsule(p2, .25 * s, .04 * s), .001 * s);

                return d;

            }


            float dstMan(float3 p, float s) {

                float speed = 10.;
                float beat = .5 * smoothstep(.0, 1., abs(sin(_Time.y * speed)));
                beat = 0.;
                //float beat = .5 * abs(sin(_Time.y * speed));
                float bbeat = .04 * sin(_Time.y * speed);
                bbeat = 0.;
                //float beat = abs(sin(_Time.y));
                float hbeat = 1. + .3 * sin(_Time.y * speed);
                float dbeat = 1. + .1 * cos(_Time.y * speed);
                float d = 0.;

                float3 a = float3(0., .25 * s, 0);
                float3 b = float3(0. + bbeat, -.7 * s, 0);

                float body = dstCapsule(p, a, b, .4 * s);


                a = float3(.25 * s, -.8 * s, 0);
                b = float3(.75 * s - beat, -1.5 * s, 0. + beat);
                float leg_r = dstCapsule(p, a, b, .25 * s);
                a = float3(-.25 * s, -.8 * s, 0);
                b = float3(-.75 * s + beat, -1.5 * s, 0. - beat);
                float leg_l = dstCapsule(p, a, b, .25 * s);

                a = float3(0., .25 * s, 0);
                b = float3(-1. * s, .25 * s - beat, 0. - beat);
                float arm_l = dstCapsule(p, a, b, .25 * s);
                a = float3(0., .25 * s, 0);
                b = float3(1. * s, .25 * s - beat, 0. - beat);
                float arm_r = dstCapsule(p, a, b, .25 * s);

                float dick = dstSphere(p - float3(0., -.85 * s, -.25 * s), .15 * s);
                float head = dstSphere(p - float3(0., .75 * s - beat * .2, 0), .5 * s + bbeat);

                float hat = dstCappedCylinder(p - float3(0, 1.3 * s, 0), .5 * s, .3 * s);
                float hat2 = dstCappedCylinder(p - float3(0, 1. * s, 0), .8 * s, .05 * s);

                a = p - float3(0, .73 * s, -.5 * s);
                a.yz = mul(a.yz,rot2d(2.));
                float nose = dstRoundCone(a, .06 * s, .03 * s, .2 * s);
                float eyel = dstSphere(p - float3(-0.2 * s, .8 * s, -.4 * s), .07 * s);
                float eyer = dstSphere(p - float3(0.2 * s, .8 * s, -.4 * s), .07 * s);

                float mouth = dstEllipsoid(p - float3(0, .55 * s, -.45 * s), float3(.16, .05, .1) * s);
                float heart = Dollar(p + float3(-0.16 * s, 0, (0.3 * s) * hbeat), s * .3);
                float euro = Euro(p + float3(0., .85 * s, (.4 * s) * dbeat), s * .4);
                d = body;
                d = smin(d, leg_l, .1 * s);
                d = smin(d, leg_r, .1 * s);
                d = smin(d, arm_l, .1 * s);
                d = smin(d, arm_r, .1 * s);

                d = smin(d, head, .1 * s);

                d = smin(d, dick, .1 * s);
                d = smin(d, hat, .1 * s);
                d = smin(d, hat2, .1 * s);
                d = smin(d, nose, .1 * s);
                d = smin(d, eyel, .01 * s);
                d = smin(d, eyer, .01 * s);
                d = smin(d, mouth, .1 * s);
                d = smin(d, heart, .1 * s);
                d = smin(d, euro, .1 * s);
                return d;

            }

            float3 TransformX(float3 p) {

                
             //   p.xy = mul(p.xy, rot2d(3.14) * .1);
                p.xz = mul(p.xz, rot2d(sin(_Time.y * 1.7) * .2));
                p.yz = mul(p.yz, rot2d(sin(_Time.y * 2.7) * .1));
              //  p.yz = mul(p.yz, rot2d(sin(_Time.y * 1.7) * .2));*/

//                p.xz = mul(p.xz, rot2d(PI));
                //p -= float3(0, 0, 0);
                p.zy = mul(p.zy, rot2d(PI)+ sin(_Time.y * 2.) * .1);
                //p.yz = mul(p.yz, sin(_Time.y*2.) *.1);
                return p;

            }

            float3 TransformMan(float3 p) {
                
                p.xyz += _MPos.xyz;
                p.z += .2 * sin(_Time.y * 2.1);
                p.xy = mul(p.xy, rot2d(cos(_Time.y * .4) * .2));
                p.zx = mul(p.xz, rot2d(1.6+ sin(_Time.y * 1.5) * .2));
                // p.yz *=rot2d(_Time.y * .7); 

                p -= float3(0, 0, 0);
                return p;

            }

            float3 TransformS(float3 p) {
                p.z +=.1+ sin(_Time.y * 2.1) *.1;
                //p.xyz += _SPos.xyz;
                p.xyz += _SPos.xyz;
                p.xy = mul(p.xy,rot2d(cos(_Time.y * 1.9) * .05));
                p.zx = mul(p.xz,rot2d(1.6+sin(_Time.y * 1.5) * .1));
                //p.yz = mul(p.yz, rot2d(cos(_Time.y * .7)*.1)); 

                //p -= float3(0, 0, 0);
                return p;

            }
            float dst_K(float3 p, float s) {

                float d = dstRoundBox(p, float3(.002 * s, .2 * s, .05 * s), .02 * s);
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(.7));
                d = smin(d, dstRoundBox(p2 + float3(0., -.13 * s, 0.), float3(.002 * s, .1 * s, .05 * s), .02 * s), .01 * s);
                p2 = p;
                p2.xy = mul(p2.xy, rot2d(-.7));
                d = smin(d, dstRoundBox(p2 + float3(0., .13 * s, 0.), float3(.002 * s, .1 * s, .05 * s), .02 * s), .01 * s);
                return d;
            }

            float dst_k(float3 p, float s) {

                float d = dstRoundBox(p, float3(.002 * s, .2 * s, .05 * s), .02 * s);
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(.7));
                d = smin(d, dstRoundBox(p2 + float3(0.07*s, 0. * s, 0.), float3(.002 * s, 0.07 * s, .05 * s), .02 * s), .01 * s);
                p2 = p + float3(0.07 * s, .14 * s, 0.);
                p2.xy = mul(p2.xy,rot2d(-.7));
                d = smin(d, dstRoundBox(p2, float3(.002 * s, 0.07 * s, .05 * s), .02 * s), .01 * s);
                return d;
            }

            float dst_X(float3 p, float s) {
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(.56));
                float d = dstRoundBox(p2, float3(.002 * s, .21 * s, .05 * s), .02 * s);
                p2 = p;
                p2.xy = mul(p2.xy, rot2d(-.56));
                d = smin(d, dstRoundBox(p2, float3(.002 * s, .21 * s, .05 * s), .02 * s), .01 * s);
                return d;
            }

            float dst_i(float3 p, float s) {
                float d = dstSphere(p - float3(0, .04 * s, 0.), .04 * s);
                d = smin(d, dstRoundBox(p - float3(0, -.11 * s, 0.), float3(.002 * s, .07 * s, 0.05 * s), .02 * s), .01 * s);
                return d;
            }


            float dst_e(float3 p, float s) {
                s *= .8;
                float3 p2 = p + float3(0, .09 * s, 0.);
                p2.yz = mul(p2.yz, rot2d(1.5));
                float d = dstRoundedCylinder(p2, .07 * s, .01 * s, .05 * s);
                float c = dstRoundedCylinder(p2, .04 * s, .01 * s, .05 * s);

                float c2 = dstRoundBox(p - float3(0.13 * s, -0.15 * s, 0.), float3(.03 * s, .07 * s, 0.05 * s), .001 * s);
                d = smax(d, -c, .1 * s);
                d = smax(d, -c2, .1) * s;
                p2 = p - float3(0.05 * s, -.1 * s, 0.);
                p2.yz = mul(p2.yz, rot2d(1.5));
                d = smin(d, dstRoundBox(p + float3(0, 0.07 * s, 0), float3(.1 * s, .00001 * s, 0.05 * s), .02 * s), 0.025 * s);
                return d;
            }

            float dst_l(float3 p, float s) {
                return dstRoundBox(p, float3(.002 * s, .2 * s, .05 * s), .02 * s);

            }
            float dst_7(float3 p, float s) {
                float d = dstRoundBox(p - float3(0.1 * s, 0.2 * s, 0.), float3(.16 * s, .002 * s, .05 * s), .02 * s);
                float3 p2 = p - float3(.06 * s, 0.01 * s, 0);
                p2.xy = mul(p2.xy, rot2d(-1.));
                d = smin(d, dstRoundBox(p2, float3(.22 * s, .002 * s, .05 * s), .02 * s), 0.01 * s);
                return d;
            }
            float xkikkel7(float3 p, float s) {
                p.xy = mul(p.xy, rot2d(PI));
                float d = dst_X(p + float3(-.87 * s, 0., 0.), s);               
                d = smin(d, dst_K(p + float3(-.64 * s, 0., 0.), s), .01 * s);
                d = smin(d, dst_i(p + float3(-.35 * s, 0., 0.), s), .01 * s);
                d = smin(d, dst_k(p + float3(-.2 * s, 0., 0.), s), .01 * s);
                d = smin(d, dst_k(p + float3(0.05 * s, 0., 0.), s), .01 * s);
                float3 p2 = p;
                p2.xz = mul(p2.xz, rot2d(PI));
                d = smin(d, dst_e(p2 - float3(0.35 * s, -0.01, 0.), s), .01 * s);
                d = smin(d, dst_l(p + float3(0.58 * s, 0.0, 0.), s), .01 * s);
                d = smin(d, dst_7(p + float3(0.95 * s, 0.0, 0.), s), .01 * s);
                return d;
            }



            RMHit GetDist(float3 p) {
                RMHit hit;
                float ta = 0;
                if ((_Time.y > 14.9) && (_Time.y <29.25)) ta = abs(sin(_Time.y * 6.)) * .013;
                hit.d = xkikkel7(TransformX(p), .7);
                //hit.d -= fbm(TransformX(p * 10.).xz) * .002;
                if (_Time.y > FADE_OUT) {
                    float rr = (_Time.y - FADE_OUT) * 0.001;
                    hit.d += fbm2(TransformX(p).xz*20.) * (0.001 + rr);
                }
                hit.d = smin(hit.d, dstMan(TransformMan(p), .16), .1);
                hit.d = smin(hit.d, skull_talk(TransformS(p), ta), .1);
                hit.id = 1;
                return hit;
            }

            RMHit RayMarch(float3 ro, float3 rd) {
                float dO = 0.;
                RMHit hit;
                hit.d = 0;
                for (int i = 0; i < MAX_STEPS; i++) {
                    float3 p = ro + rd * dO;
                    hit = GetDist(p);

                    dO += hit.d;
                    if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;

                }
                hit.d = dO;
                return hit;
            }

            float3 GetNormal(float3 p) {
                float d = GetDist(p).d;

                float2 e = float2(0.01, 0);
                float3 n = d - float3(
                    GetDist(p - e.xyy).d,
                    GetDist(p - e.yxy).d,
                    GetDist(p - e.yyx).d);
                return normalize(n);

            }


            float GetLight(float3 p, float3 lightPos) {

                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p);
                float dif = clamp(dot(n, l), 0., 1.);
                float d = RayMarch(p + n * SURF_DIST * 3., l).d;
                if (d < length(lightPos - p)) dif *= .1; // shadow
                return dif;
            }

            float3 GetRayDir(float2 uv, float3 p, float3 l, float z) {
                float3 f = normalize(l - p),
                    r = normalize(cross(float3(0, 1, 0), f)),
                    u = cross(f, r),
                    c = f * z,
                    i = c + uv.x * r + uv.y * u,
                    d = normalize(i);
                return d;
            }

        
            fixed4 frag(v2f i) : SV_Target
            {
                //i.uv = 1. - i.uv;
                //i.uv.y = 1 - i.uv.y;
                i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
               
                
                float3 col = 0;
                float3 ro = float3(0., 0., -1.);

                float3 rd = GetRayDir(uv, ro, 0, 1.);
                rd = normalize(float3(uv.x, uv.y, 1.));
                RMHit hit = RayMarch(ro, rd);
                col += Bgr(uv);
                if (hit.d < MAX_DIST) {
                    float3 p = ro + rd * hit.d;
                    float3 n = GetNormal(p);
                    float3 r = reflect(rd, n);
                    float spec = pow(max(0., r.y), 30.);
                    //float dif = dot(n, normalize(float3(1, 2, 3))) * .5 + .5;

                    float l = GetLight(p, float3(0, 0, -3));
                    col = lerp(Bgr(r), l, .4) + spec;

                }

                col = pow(col, .4545);
                col = lerp(0, col, _Amount);
                return fixed4(col, 1);
            }
            ENDCG
        }

    }
}
