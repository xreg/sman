﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SecondMainCamera : SceneCameraBase
{
    
    public GameObject bgr;
    public int fadeState = 0;
    Material mat = null;
    Vector4 _spos = new Vector4(1.5f, 0, 0, 0);
    Vector4 _mpos = new Vector4(1.5f, 0, 0, 0);
    private float amount = 1;
    void Start()
    {
        base.Start();
        mat = bgr.GetComponent<Renderer>().material;
        mat.SetFloat("_Amount", 0f);
        mat.SetVector("_MPos", _mpos);
        mat.SetVector("_SPos", _spos);
    }

    // Update is called once per frame
    public float xpos = 0;
    float ypos = 0;
    void Update()
    {
        timer += Time.deltaTime;
        if (fadeState == 0)
        {
            mat.SetFloat("_Amount", timer);
            if (timer > 1f)
            {
                fadeState++;
            }
        }
        if (fadeState == 1)
        {
            
            _mpos.x -= Time.deltaTime * .4f;
            _mpos.y = Mathf.Sin(timer*1.5f) * .1f;
            mat.SetVector("_MPos", _mpos);
            if (timer > 8)
            {
                fadeState++;
                xpos = 0;
            }


        }

        if (fadeState == 2)
        {            
            
            _spos.x -= Time.deltaTime * .3f;
            mat.SetVector("_SPos", _spos);
            if (timer > 13)
            {
                fadeState++;
            }
        }

        if ((fadeState == 3) && (timer > (SceneTimes.SecondScene - 1)))
        {
            fadeState++;
        }
        if (timer > SceneTimes.SecondScene)
        {
            SceneManager.LoadScene("FbmFireScene");
        }
        if (fadeState == 4)
        {
            amount -= Time.deltaTime;
            mat.SetFloat("_Amount", amount);
        }

    }
}
