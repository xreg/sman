using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShadeDotsMainCam : SceneCameraBase
{
    public GameObject bgr;
    int fadeState = 0;
    Material mat = null;
    //private float timer = 0;
    public float amount = 0;
    void Start()
    {
        base.Start();
        mat = bgr.GetComponent<Renderer>().material;
        mat.SetFloat("_Amount", 0f);
        
    }

    
    void Update()
    {
        timer += Time.deltaTime;
        
        if (timer > SceneTimes.ShadeDots)
        {
            
            SceneManager.LoadScene("WobbleScene");
        }
        if (fadeState == 0)
        {
            mat.SetFloat("_Amount", timer);
            if (timer > 1f)
            {
                mat.SetFloat("_Amount", 1);
                fadeState++;
            }

        }
        
       
        if ((fadeState == 1) && (timer > SceneTimes.ShadeDots - 1))
        {
            
            fadeState++;
        }
        if (fadeState == 2)
        {
            mat.SetFloat("_Amount", amount);
            amount -= Time.deltaTime;
        }
    }
}
