﻿Shader "ShadeDotsOLD"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Amount("Amount",float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" } 
         GrabPass { "_GrabTexture" }


        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag          
            #include "UnityCG.cginc"
            #include "../XCGInclude.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;                
                float4 grabUv : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _GrabTexture;
            float _Amount;
            float Star(float2 p) {
                float l = length(p);
                float c = 0.2 / l;
                c *= smoothstep(1., 0., l);
                return c;
            }

            float3 randomStar(float2 uv) {
                uv *= 15.;
                float2 f_uv = frac(uv);
                float2 i_uv = floor(uv);
                float3 col = 0;


                int ls = 2;
                for (int y = -ls; y <= ls; y++) {
                    for (int x = -ls; x <= ls; x++) {

                        float2 off = float2(x, y);
                        float s = rand2d(i_uv + off);
                        float2 mv = float2(rand2d((i_uv + off) * 242.49), rand2d(i_uv + off));
                        mv.x *= 1.5 * sin(_Time.y * rand2d(i_uv + off));
                        mv.y *= 1.5 * cos(_Time.y * rand2d(i_uv + off));
                        col += Star(f_uv - off - mv) * s; // *smoothstep(.2, .51, frac(_Time.y * rand2d(i_uv + off)))* s;
                        //float2 cr = rand2dv(i_uv + off);
                        //float3 c = sin(float3(cr.x, 0., cr.y));
                        
                        

                    }
                }
                col.r *= rand2d(i_uv);
                col.g *= rand2d(i_uv) * .5;
                col.b *= rand2d(i_uv) * .25;
                //col.b *= .3 * smoothstep(0., 1., frac(_Time.y * rand2d(i_uv)));
                return col;
            }

       
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.grabUv = ComputeGrabScreenPos(o.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
                uv = mul(uv, rot2d(cos(_Time.y *.3)));
                uv *= 3. + 2 * cos(_Time.y * .267);
                float3 col = 0;
                //i.grabUv.xy = mul(i.grabUv.xy, rot2d(sin(_Time.y * .13) * cos(_Time.y * .022)));
                col += tex2Dproj(_GrabTexture, i.grabUv).rgb;
                
                col += randomStar(uv);
                if (col.r >= 0.0) {
                    float d = .02;
                    col += tex2Dproj(_GrabTexture, i.grabUv).rgb;
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(d, d, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(d, -d, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(-d, d, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(-d, -d, 0, 0));
                    col /= 5.7;


                }

                if (col.r < 0.1) col = 0.;
                
                return fixed4(col, 1);
            }
            ENDCG
        }
    
        GrabPass{ "_GrabTexture2" }

            Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
             #include "../XCGInclude.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 grabUv : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _GrabTexture2;
            float _Amount;
            float Star(float2 p) {
                float l = length(p);
                float c = .2 / l;
                c *= smoothstep(1., 0.1, l);

                return c;
            }

            float3 randomStar(float2 uv) {
                uv *= 3.;
                float2 f_uv = frac(uv);
                float2 i_uv = floor(uv);
                float3 col = 0;


                int ls = 2;
                for (int y = -ls; y <= ls; y++) {
                    for (int x = -ls; x <= ls; x++) {

                        float2 off = float2(x, y);
                        float s = smoothstep(.8, 1., rand2d(i_uv + off)); // SIZE
                        float2 mv = float2(rand2d((i_uv + off) * 242.49), rand2d(i_uv + off));
                        mv.x *= 1. * sin(_Time.y * rand2d(i_uv + off));
                        mv.y *= 1. * cos(_Time.y * rand2d(i_uv + off));
                        col += Star(f_uv - off - mv) *smoothstep(.4, .61, frac(_Time.y * rand2d(i_uv + off)))* s;
                    }
                }
                col.r *= 2.; // rand2d(i_uv) * 1.2;
                col.g *= 1; // rand2d(i_uv * 21.134) * .9;
                col.b *= rand2d(i_uv) * .001;
                //col.b *= .3 * smoothstep(0., 1., frac(_Time.y * rand2d(i_uv)));
                return col;
            }

         
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.grabUv = ComputeGrabScreenPos(o.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
                uv = mul(uv, rot2d(sin(_Time.y)));
                uv *= 3. + 2 * sin(_Time.y * .267);
                float3 col = 0;
                //i.grabUv.xy = mul(i.grabUv.xy, rot2d(sin(_Time.y * .1) * cos(_Time.y * .12)));
                col = tex2Dproj(_GrabTexture2, i.grabUv).rgb;
                col -= randomStar(uv);
                
                if (col.r >= 0.0) {
                    float d = .01;
                    col += tex2Dproj(_GrabTexture2, i.grabUv).rgb;
                    col += tex2Dproj(_GrabTexture2, i.grabUv + float4(d, d, 0, 0));
                    col += tex2Dproj(_GrabTexture2, i.grabUv + float4(d, -d, 0, 0));
                    col += tex2Dproj(_GrabTexture2, i.grabUv + float4(-d, d, 0, 0));
                    col += tex2Dproj(_GrabTexture2, i.grabUv + float4(-d, -d, 0, 0));
                    col /= 6.4;


                }

                if (col.r < 0.1) col = 0.;
                col = lerp(0., col, _Amount);
                return fixed4(col, 1);
            }
            ENDCG
        }

    }
}
