﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTimes : MonoBehaviour
{
    public const float IntroScene = 16f; // dollar euro zoomer    
    public const float SecondScene = 32; //48  xkikkel logo
    public const float FbmFire2Scene = 25; // 73   dollar and euro sign moving, fire world bgr
    public const float BlackSunScene = 16f;//89
    public const float WarScene = 35f;// 119
    public const float OldSkoolPlasma = 29; //
    public const float TunnelScene = 32;
    public const float StarCloud = 14;
    public const float ShadeDots = 27;
    public const float Wobble = 20;
    public const float SkullDie = 30;
}
