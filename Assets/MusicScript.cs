﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class MusicScript : MonoBehaviour {
    

    // Use this for initialization
    public AudioSource audioData;
    private static MusicScript instance;
    public static MusicScript GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        Debug.Log("AWAKE!!!");
       
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            Debug.Log(" DESTORY ");
            return;
        }
        else
        {
            Debug.Log("  DONT DESTROY DESTORY ");
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
    void Start () {
        audioData = GetComponent<AudioSource>();
        
        audioData.Play(0);


    }

    // Update is called once per frame
    void Update () {
        //Debug.Log(" audio time in main camera: " + audioData.time);
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

    }
}
