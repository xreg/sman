using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TunnelSceneMainCam : SceneCameraBase
{
    public GameObject bgr;
    int fadeState = 0;
    Material mat = null;

    public float amount = 1;


    void Start()
    {
        base.Start();
        mat = bgr.GetComponent<Renderer>().material;
        mat.SetFloat("_Amount", 0f);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > SceneTimes.TunnelScene)
        {
            SceneManager.LoadScene("StarCloudScene");
                
        }
        if (fadeState == 0)
        {
            mat.SetFloat("_Amount", timer);
            if (timer > 1f)
            {
                mat.SetFloat("_Amount", 1);
                fadeState++;
            }

        }

        if ((fadeState == 1) && (timer > SceneTimes.TunnelScene - 1))
        {
            fadeState++;
        }
        if (fadeState == 2)
        {
            mat.SetFloat("_Amount", amount);
            amount -= Time.deltaTime;
        }

    }
}
