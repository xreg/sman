Shader "TunnelShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Amount("Amount",float) = 0

    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
          
            #include "UnityCG.cginc"
            #include "../XCGInclude.cginc"
            #define MAX_STEPS 40
            #define MAX_DIST 100.
            #define SURF_DIST .001
            #define PI 3.14159

            struct RMHit {
                float d;
                int id;
            };
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
              
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float fbm(float2 p) {

                float freq = 1.0;
                float amp = 1.0;
                float v = 0.;
                for (int i = 0; i < 6; i++) {
                    float x = sin(float(i) + _Time.y * .79);
                    float y = cos(_Time.y);
                    //mat2 R = mat2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    float2x2 R = float2x2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    v += amp * noise2d(freq * mul(R, p) + x + y);
                    freq *= 2.;
                    amp *= .5;
                }
                return v;
            }
            float Line(float2 p, float size) {
                float v = 0.;                
                v = length(p.y * size);
                float c = .2 / v;
                c *= smoothstep(.89999, .10001, v);
                return c;

            }
            float dstPart(float3 p, float s) {
                float st = .2;
                float r = .02;
                float du = dstRoundBox(p - float3(0., st, 0.), float3(1. * s, .1 * s, .1 * s), r);
                float dd = dstRoundBox(p + float3(0., st, 0.), float3(1. * s, .1 * s, .1 * s), r);
                float dl = dstRoundBox(p - float3(st, .0, 0.), float3(.1 * s, 1. * s, .1 * s), r);
                float dr = dstRoundBox(p + float3(st, .0, 0.), float3(.1 * s, 1. * s, .1 * s), r);
                float d = smin(du, dd, .1);
                d = smin(d, dl, .1);
                d = smin(d, dr, .1);
                return d;

            }
            float fbm2(float2 p) {

                float freq = 1.;
                float amp = 1.;
                float v = 0.;
                for (int i = 0; i < 6; i++) {
                    float x = float(i);
                    float y = cos(i * 2.);
                    float2x2 R = float2x2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    v += amp * noise2d(x + y + mul(p, R) * freq);
                    freq *= 2.;
                    amp *= .5;
                }
                return v;
            }
            RMHit GetDist(float3 p) {
                RMHit hit;

                float d = 1000.;
                float bd = 0.;
                float num_l = 20.;
                float t = 42+_Time.y * .1;
                for (float i = 0.; i <= 1.; i += 1. / num_l) {
                    float zd = frac(i + t);
                    float3 p2 = p;
                    p2.yx = mul(p2.yx, rot2d(_Time.y * i));
                    p2 += float3(cos(_Time.y + i) * .06, sin(i + _Time.y) * .06, 0. + zd * 2.2);
                    bd = dstPart(p2, .15);

                    d = smin(d, bd, .125);
                }

                float dp1 = 3. - p.y;
                d = min(d, dp1);
                float dp2 = p.y + 3.;
                d = min(d, dp2);
               
                ////float md = dstMan(TransformMan(p), .05);
                //float f = (fbm2(p.xz * 10.)) * 0.01;
                //d -= f;
                //f = (fbm2(p.xz * 7.)) * 0.02;
                //d += f;

                hit.id = 3;
                hit.d = d;
                //hit.d = dstRoundBox(p-float3(0,0,2), float3(1, 1, 1), 0.1);
                //hit.d = dstPart(p, .1);
                ///hit.d = min(md, d);
                //if (hit.d == d) hit.id = 3;
                //if (hit.d == md) hit.id = 2;


                //float bd = dstPart(TransformPart(p), .2);
                //float sd = dstSphere(TransformS(p), 1.);
                //float st = dstTorus(TransformT(p), .2, .1);
                ///hit.d = min(bd, sd);
                //hit.d = min(hit.d, st);
                //p+=float3(0,0,-3);
                //d = dstRBox(p, float3(1.1,.1,.1), .2);

                //if (d == hit.d) hit.id = 1;
                //if (sd == hit.d) hit.id = 2;
                //if (st == hit.d) hit.id = 3;
                return hit;
            }

            RMHit RayMarch(float3 ro, float3 rd) {
                float dO = 0.;
                RMHit hit;
                for (int i = 0; i < MAX_STEPS; i++) {
                    float3 p = ro + rd * dO;
                    hit = GetDist(p);

                    dO += hit.d;
                    if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;
                }
                hit.d = dO;
                return hit;
            }

            float3 GetNormal(float3 p) {
                float d = GetDist(p).d;
                float2 e = float2(0.01, 0);

                float3 n = d - float3(
                    GetDist(p - e.xyy).d,
                    GetDist(p - e.yxy).d,
                    GetDist(p - e.yyx).d);
                return normalize(n);

            }

            float GetLight(float3 p, float3 lightPos) {

                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p);
                float dif = clamp(dot(n, l), 0., 1.);
                float d = RayMarch(p + n * SURF_DIST * 3., l).d;
                //if (d<length(lightPos-p)) dif *= .1; // shadow
                return dif;
            }

           

            float3 bgr(float2 uv) {
                float2 o_uv = uv;
                uv.y *= 6.;
                float3 col = 0;
                float2 i_uv = floor(uv);
                float2 f_uv = frac(uv);
             
                float ls = 6.;
                float ls2 = 0.;
                float2 v = 0;
                float2 w = 0;
                float2 fbm_uv = uv;
               
                for (float y = -ls; y <= ls; y += 1.) {
                    for (float x = -ls2; x <= ls2; x += 1.) {
                        float2 off = float2(x, y);
                        float2 uv_off = i_uv + off;
                        float2 offp = float2(x, rand2d(uv_off));

                        v.x = fbm(i_uv + fbm_uv + off);

                        v.y = fbm(i_uv + fbm_uv + v.x + off);
                        w.x = fbm(i_uv + fbm_uv + v.y + off);
                        w.x *= .6 + .55 * sin(_Time.y);
                        float2 p = f_uv - off - offp - float2(0, 3. * sin(_Time.y * rand2d(uv_off) + rand2d(uv_off) * 2. * PI));
                      
                        col += lerp(float3(0, 0, 0.), float3(.2, .0, .9), Line(p + w.x, .3 + rand2d(uv_off) / .8));
                    }
                }

                // Output to screen
                col = lerp(col, float3(.2, 0., .05), o_uv.y / 2. + .25);
                col = lerp(col, float3(.05, 0., .1), .75 - (o_uv.y / 2. + .25));
                return col;
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
               
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                 i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
                float3 col = 0;
                float3 ro = float3(0, 0, -2);

                
                float3 rd = normalize(float3(uv.x, uv.y, 1.));
                
                RMHit hit = RayMarch(ro, rd);

                if (hit.d < MAX_DIST) {

                    if ((hit.id == 3)) {
                        float3 p = ro + rd * hit.d;
                        float3 n = GetNormal(p);
                        float3 r = reflect(rd, n);
                        float spec = pow(max(0., r.y), 30.);
                        //if (hit.id == 1) BOX MAT
                        //if (hit.id == 2) SPHHERE MAT
                        //float dif = dot(n, normalize(float3(1, 2, 3))) * .5 + .5;
                        //float l = GetLight(p, float3(sin(_Time.y) * 5.,0,cos(_Time.y) * 2.));
                        float l = GetLight(p, float3(0,0,-3));
                        //float3 c = texture(iChannel0, p.xz).rgb;
                        col = bgr(p.zy).rgb;
                        //col = l*0.01;


                    }
                    //else if (hit.id == 2) {
                    //    float3 p = ro + rd * hit.d;
                    //    float3 n = GetNormal(p);
                    //    float3 r = reflect(rd, n);
                    //    float spec = pow(max(0., r.y), 30.);
                    //    //if (hit.id == 1) BOX MAT
                    //    //if (hit.id == 2) SPHHERE MAT
                    //    float dif = dot(n, normalize(float3(1, 2, 3))) * .5 + .5;
                    //    //float l = GetLight(p, float3(sin(_Time.y) * 5.,0,cos(_Time.y) * 2.));
                    //    float l = GetLight(p, float3(0, 0, -3));


                    //    col = lerp(bgr(p.zy).rgb, float3(l), .5) + spec;
                    //}
                    //if (hit.id != 2)
                        col += hit.d / 2.;


                    //col.r += hit.d / 2.;    
                    //col.b += hit.d / 3.;    
                    //col.g += hit.d / 4.;    

                }
                
                //col = pow(col, float3(.
                return fixed4(col, 1);
            }
            ENDCG
        }
        GrabPass{ "_GrabTexture"}
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "../XCGInclude.cginc"
            #define MAX_STEPS 40
            #define MAX_DIST 100.
            #define SURF_DIST .001
            struct RMHit {
                float d;
                int id;
            };
            
            float Dollar(float3 p, float s) {
                float d = dstCappedTorus(p - float3(0, .2 * s, 0), float2(1. * s, .0), .2 * s, .05 * s);
                float3 p2 = p + float3(0, .2 * s, 0);
                p2.xy = mul(p2.xy, rot2d(3.14));
                d = smin(d, dstCappedTorus(p2, float2(1. * s, 0.), .2 * s, .05 * s), .1 * s);
                p2 = p;
                p2.y += 0.2 * s;
                p2.xy = mul(p2.xy, rot2d(.8));
                d = smin(d, dstCappedTorus(p2, float2(.4 * s, 0.3 * s), .2 * s, .05 * s), .001 * s);
                p2 = p;
                p2.y -= 0.2 * s;
                p2.xy = mul(p2.xy, rot2d(4.14));
                d = smin(d, dstCappedTorus(p2, float2(.4 * s, 0.3 * s), .2 * s, .05 * s), .001 * s);
                d = smin(d, dstVerticalCapsule(p + float3(0, .45 * s, 0), .9 * s, .04 * s), .001 * s);
                return d;
            }

            float Euro(float3 p, float s) {
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(-1.5));
                float d = dstCappedTorus(p2, float2(1. * s, .0), .2 * s, .05 * s);
                p2 = p;
                p2.x += -.25 * s;
                p2.y += -.05 * s;
                p2.xy = mul(p2.xy, rot2d(1.57));
                d = smin(d, dstVerticalCapsule(p2, .25 * s, .04 * s), .001 * s);
                p2.x += .1 * s;
                d = smin(d, dstVerticalCapsule(p2, .25 * s, .04 * s), .001 * s);

                return d;

            }
            float dstMan(float3 p, float s) {

                float speed = 8.;
                float beat = 0.;                                
                float bbeat = .02*sin(_Time.y*3.2);               
                
                float hbeat = 1. + .6 * sin(_Time.y * speed);
                float dbeat = 1. + .4 * cos(_Time.y * speed);
                float d = 0.;

                float3 a = float3(0., .25 * s, 0);
                float3 b = float3(0., -.7 * s, 0);

                float body = dstCapsule(p, a, b, .4 * s);

                a = float3(.25 * s, -.8 * s, 0);
                b = float3(.75 * s - bbeat*.8, -1.5 * s, 0. + bbeat*.7);
                float leg_r = dstCapsule(p, a, b, .25 * s);
                a = float3(-.25 * s, -.8 * s, 0);
                b = float3(-.75 * s + bbeat*.6, -1.5 * s, 0. - bbeat*.8);
                float leg_l = dstCapsule(p, a, b, .25 * s);

                a = float3(0., .25 * s, 0);
                b = float3(-1. * s, .25 * s + bbeat *.8, 0. - bbeat);
                float arm_l = dstCapsule(p, a, b, .25 * s);
                a = float3(0., .25 * s, 0);
                b = float3(1. * s, .25 * s - bbeat * .7, 0. + bbeat);
                float arm_r = dstCapsule(p, a, b, .25 * s);

                float dick = dstSphere(p - float3(0.,-.85 * s,-.25 * s), .15 * s);
                float head = dstSphere(p - float3(0., .75 * s - beat * .2, 0), .5 * s);
                //float dstCappedCylinder( float3 p, float h, float r )
                float hat = dstCappedCylinder(p - float3(0,1.3 * s, 0), .5 * s, .3 * s);
                float hat2 = dstCappedCylinder(p - float3(0,1. * s, 0), .8 * s, .05 * s);
                //float dstRoundCone( float3 p, float r1, float r2, float h )
                a = p - float3(0, .83 * s,-.8 * s);
                a.yz = mul(a.yz, rot2d(2.));
                float nose = dstRoundCone(a, .01 * s , .03 * s, .8 * s);
                float eyel = dstSphere(p - float3(-0.2 * s,.8 * s,-.4 * s), .1 * s);
                float eyer = dstSphere(p - float3(0.2 * s,.8 * s,-.4 * s), .1 * s);
                //float dstEllipsoid( float3 p, float3 r )
                float mouth = dstEllipsoid(p - float3(0, .55 * s, -.45 * s), float3(.16, .05,.2) * s);
                float3 p2 = p + float3(-0.19 * s, 0, (0.3 * s) * hbeat);
                p2.xz = mul(p2.xz, rot2d(3.14159));
                float heart = Dollar(p2 , s * .4);
                p2 = p + float3(0., .85 * s, (.4 * s) * dbeat);
                p2.xy = mul(p2.xy, rot2d(3.14159));
                float euro = Euro(p2 , s * .5);
                d = body;
                d = smin(d, leg_l, .1 * s);
                d = smin(d, leg_r, .1 * s);
                d = smin(d, arm_l, .1 * s);
                d = smin(d, arm_r, .1 * s);

                d = smin(d, head, .1 * s);
                //d = smin(d, pack, .1 * s);
                d = smin(d, dick, .1 * s);
                d = smin(d, hat, .1 * s);
                d = smin(d, hat2, .1 * s);
                d = smin(d, nose, .01 * s);
                d = smax(d, -eyel, .1 * s);
                d = smax(d, -eyer, .1 * s);
                d = smax(d, -mouth, .1 * s);
                d = smin(d, heart, .02 * s);
                d = smin(d, euro, .02 * s);
                return d;

            }
            float3 TransformMan(float3 p) {
                p += float3(0. ,0, 1.7);
               
                p.xy = mul(p.xy, rot2d(cos(_Time.y) * .35));
                //p.xz = mul(p.zx, rot2d(3.14159*1.5+sin(_Time.y*1.34) * .15));
                p.zx = mul(p.zx, rot2d(sin(_Time.y*2.) * .35));
                return p;
             }
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 grabUv : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _GrabTexture;
            float _Amount;
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.grabUv = ComputeGrabScreenPos(o.vertex);
                return o;
            }
            RMHit GetDist(float3 p) {
                RMHit hit;

                hit.d = dstMan(TransformMan(p), .05);
                hit.id = 2;                
                return hit;
            }

            RMHit RayMarch(float3 ro, float3 rd) {
                float dO = 0.;
                RMHit hit;
                for (int i = 0; i < MAX_STEPS; i++) {
                    float3 p = ro + rd * dO;
                    hit = GetDist(p);

                    dO += hit.d;
                    if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;
                }
                hit.d = dO;
                return hit;
            }

            float3 GetNormal(float3 p) {
                float d = GetDist(p).d;
                float2 e = float2(0.01, 0);

                float3 n = d - float3(
                    GetDist(p - e.xyy).d,
                    GetDist(p - e.yxy).d,
                    GetDist(p - e.yyx).d);
                return normalize(n);

            }

            float GetLight(float3 p, float3 lightPos) {

                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p);
                float dif = clamp(dot(n, l), 0., 1.);
                float d = RayMarch(p + n * SURF_DIST * 3., l).d;
                //if (d<length(lightPos-p)) dif *= .1; // shadow
                return dif;
            }


            fixed4 frag(v2f i) : SV_Target
            {
                 i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
                float3 col = 0;
                col = tex2Dproj(_GrabTexture, i.grabUv).rgb;
             
                float3 ro = float3(0, 0, -2);


                float3 rd = normalize(float3(uv.x, uv.y, 1.));

                RMHit hit = RayMarch(ro, rd);

                if (hit.d < MAX_DIST) {

                    if ((hit.id == 2)) {
                        float3 p = ro + rd * hit.d;
                        float3 n = GetNormal(p);
                        float3 r = reflect(rd, n);
                        float spec = pow(max(0., r.y), 30.);
                        
                        //float dif = dot(n, normalize(float3(1, 2, 3))) * .5 + .5;
                        //float l = GetLight(p, float3(sin(_Time.y) * 5.,0,cos(_Time.y) * 2.));
                        float l = GetLight(p, float3(0, 0, -3));
                        //float3 c = texture(iChannel0, p.xz).rgb;
                        i.grabUv.x = r.x;
                        i.grabUv.y = r.z;
                        float3 c = tex2Dproj(_GrabTexture, i.grabUv).rgb;
                        col = lerp(c, l, .5) + spec;
                      


                    }
                

                }
                col = lerp(0., col, _Amount);
                return fixed4(col, 1);
            }
            ENDCG
        }
    }
}
