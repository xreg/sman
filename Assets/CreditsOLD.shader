Shader "CreditsOLD"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            #include "XCGInclude.cginc"
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
            
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            #define MAX_STEPS 40
            #define MAX_DIST 100.
            #define SURF_DIST .001
            #define PI 3.14159
            struct RMHit {
                float d;
                int id;
            };
            float dstC(float3 p, float s) {
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(-1.56));
                float d = dstCappedTorus(p2, float2(1. * s, .0), .1 * s, .05 * s);
                return d;

            }
            float dstO(float3 p, float s) {
                float3 p2 = p;
                p2.yz = mul(p2.yz, rot2d(-1.56));
                float d = dstTorus(p2, float2(.11 * s, .05 * s));
                return d;
            }

            float dstD(float3 p, float s) {
                float d = dstVerticalCapsule(p + float3(0, .1 * s, 0), .2 * s, .05 * s);
                float3 p2 = p - float3(.02 * s, 0, 0);
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstCappedTorus(p2, float2(1. * s, .0), .1 * s, .05 * s), .001 * s);
                return d;
            }

            float dstE(float3 p, float s) {
                float3 p2 = p;
                float d = dstVerticalCapsule(p + float3(0, .1 * s, 0), .18 * s, .05 * s);

                p2.y -= .1 * s;
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstVerticalCapsule(p2, .15 * s, .05 * s), .01 * s);
                p2 = p;
                p2.x -= 0.01;

                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstVerticalCapsule(p2, .1 * s, .05 * s), .01 * s);
                p2 = +p;
                p2.x -= 0.01;
                p2.y += .1 * s;
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstVerticalCapsule(p2, .15 * s, .05 * s), .01 * s);
                return d;

            }

            float dstSemiC(float3 p, float s) {
                float d = dstSphere(p - float3(0, .05 * s, 0), .05 * s);
                d = min(d, dstSphere(p + float3(0, .05 * s, 0), .05 * s));
                return d;
            }

            float dstX(float3 p, float s) {
                float3 p2 = p;
                p2.y += .1 * s;
                p2.xy = mul(p2.xy, rot2d(.6));
                float d = dstVerticalCapsule(p2, .24 * s, .05 * s);
                p2 = p;
                p2.y += .1 * s;
                p2.x -= .13 * s;
                p2.xy = mul(p2.xy, rot2d(-.6));
                d = smin(d, dstVerticalCapsule(p2, .24 * s, .05 * s), .01 * s);
                return d;
            }

            float dstR(float3 p, float s) {
                float d = dstVerticalCapsule(p + float3(0, .1 * s, 0), .2 * s, .05 * s);
                float3 p2 = p - float3(.04 * s, 0.05 * s, 0);
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstCappedTorus(p2, float2(1. * s, .0), .05 * s, .05 * s), .001 * s);
                p2 = p;
                p2.x -= .1 * s;
                p2.y += .1 * s;
                p2.xy = mul(p2.xy, rot2d(-.6));
                d = smin(d, dstVerticalCapsule(p2, .1 * s, .05 * s), 0.01 * s);
                return d;
            }

            float dstG(float3 p, float s) {
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(-1.56));
                float d = dstCappedTorus(p2, float2(1. * s, .0), .1 * s, .05 * s);
                p2 = p;
                p2.y += .1 * s;
                p2.x -= .03 * s;
                d = smin(d, dstVerticalCapsule(p2, .05 * s, .05 * s), 0.01 * s);
                p2 = p;
                p2.y += .01 * s;
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstVerticalCapsule(p2, .03 * s, .05 * s), 0.01 * s);
                p2 = p;
                p2.y -= .1 * s;
                p2.xy = mul(p2.xy, rot2d(1.6));
                d = smin(d, dstVerticalCapsule(p2, .04 * s, .05 * s), 0.01 * s);
                return d;
            }

            float dstM(float3 p, float s) {
                float d = dstVerticalCapsule(p + float3(0, .1 * s, 0), .2 * s, .05 * s);
                float3 p2 = p;
                p2.y += .04 * s;
                p2.x -= .1 * s;
                p2.xy = mul(p2.xy, rot2d(-.6));
                d = smin(d, dstVerticalCapsule(p2, .16 * s, .05 * s), 0.01 * s);
                p2 = p;
                p2.y += .04 * s;
                p2.x -= .1 * s;
                p2.xy = mul(p2.xy, rot2d(.6));
                d = smin(d, dstVerticalCapsule(p2, .16 * s, .05 * s), 0.01 * s);
                d = smin(d, dstVerticalCapsule(p + float3(-.2 * s, .1 * s, 0), .16 * s, .05 * s), 0.01 * s);
                return d;
            }

            float dstU(float3 p, float s) {
                float3 p2 = p;
                p2.y += .02;
                p2.xy = mul(p2.xy, rot2d(PI));
                float d = dstCappedTorus(p2, float2(1. * s, .0), .08 * s, .05 * s);
                d = smin(d, dstVerticalCapsule(p + float3(0.08 * s, .03 * s, 0), .12 * s, .05 * s), 0.01 * s);
                d = smin(d, dstVerticalCapsule(p + float3(-0.08 * s, .03 * s, 0), .12 * s, .05 * s), 0.01 * s);
                return d;
            }
            float dstS(float3 p, float s) {
                float3 p2 = p;
                p2.y -= .05 * s;
                p2.xy = mul(p2.xy, rot2d(-1.56));
                float d = dstCappedTorus(p2, float2(1. * s, -.2 * s), .0525 * s, .04 * s);
                p2 = p;
                p2.x += .03 * s;
                p2.y += .05 * s;
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstCappedTorus(p2, float2(1. * s, -.2 * s), .0525 * s, .04 * s), 0.02 * s);
                return d;
            }

            float dstI(float3 p, float s) {
                float d = dstVerticalCapsule(p + float3(0, .1 * s, 0), .08 * s, .05 * s);
                d = min(d, dstSphere(p - float3(0, .1 * s, 0), .05 * s));
                return d;
            }

            float dstK(float3 p, float s) {
                float d = dstVerticalCapsule(p + float3(0, .1 * s, 0), .2 * s, .05 * s);
                float3 p2 = p;
                p2.x -= .12;
                p2.y += .1;
                p2.xy = mul(p2.xy, rot2d(-.8));
                d = smin(d, dstVerticalCapsule(p2, .14 * s, .05 * s), 0.01 * s);
                p2 = p;
                p2.x -= .02;
                p2.xy = mul(p2.xy, rot2d(.8));
                d = smin(d, dstVerticalCapsule(p2, .14 * s, .05 * s), 0.01 * s);
                return d;
            }
            float dstN(float3 p, float s) {
                float d = dstVerticalCapsule(p + float3(0, .1 * s, 0), .2 * s, .05 * s);
                float3 p2 = p;
                p2.x -= .14 * s;
                p2.y += .1 * s;
                p2.xy = mul(p2.xy, rot2d(-.6));
                d = smin(d, dstVerticalCapsule(p2, .24 * s, .05 * s), 0.01 * s);
                d = smin(d, dstVerticalCapsule(p - float3(.16 * s, -.1 * s, 0), .2 * s, .05 * s), 0.01 * s);
                return d;
            }
            float dstL(float3 p, float s) {
                float d = dstVerticalCapsule(p + float3(0, .1 * s, 0), .2 * s, .05 * s);
                float3 p2 = p;
                p2.x -= .12 * s;
                p2.y += .1 * s;
                p2.xy = mul(p2.xy, rot2d(-1.56));
                d = smin(d, dstVerticalCapsule(p2, .1 * s, .05 * s), 0.01 * s);
                return d;
            }
            float dstV(float3 p, float s) {
                float3 p2 = p;
                p2.y += .1 * s;
                p2.xy = mul(p2.xy, rot2d(-.4));
                float d = dstVerticalCapsule(p2, .23 * s, .05 * s);
                p2 = p;
                p2.y += .1 * s;
                p2.xy = mul(p2.xy, rot2d(.4));
                d = smin(d, dstVerticalCapsule(p2, .23 * s, .05 * s), 0.01 * s);
                return d;

            }

            float dstW(float3 p, float s) {
                float3 p2 = p;
                p2.y += .1 * s;
                p2.xy = mul(p2.xy, rot2d(-.4));
                float d = dstVerticalCapsule(p2, .23 * s, .05 * s);
                p2 = p;
                p2.y += .1 * s;
                p2.xy = mul(p2.xy, rot2d(.4));
                d = smin(d, dstVerticalCapsule(p2, .23 * s, .05 * s), 0.01 * s);
                p2 = p;
                p2.y += .1 * s;
                p2.x -= .2 * s;
                p2.xy = mul(p2.xy, rot2d(-.4));
                d = smin(d, dstVerticalCapsule(p2, .23 * s, .05 * s), 0.01 * s);
                p2 = p;
                p2.y += .1 * s;
                p2.x -= .2 * s;
                p2.xy = mul(p2.xy, rot2d(.4));
                d = smin(d, dstVerticalCapsule(p2, .23 * s, .05 * s), 0.01 * s);
                return d;
            }

            float dstA(float3 p, float s) {
                float3 p2 = p;
                p2.y += .12 * s;
                p2.xy = mul(p2.xy, rot2d(.4));
                float d = dstVerticalCapsule(p2, .24 * s, .05 * s);
                p2 = p;
                p2.y += .12 * s;
                p2.x -= .2 * s;
                p2.xy = mul(p2.xy,rot2d(-.4));
                d = smin(d, dstVerticalCapsule(p2, .24 * s, .05 * s), 0.01 * s);
                p2 = p;
                p2.y += .06 * s;
                p2.x -= .05 * s;
                p2.xy = mul(p2.xy, rot2d(1.56));
                d = smin(d, dstVerticalCapsule(p2, .1 * s, .05 * s), 0.01 * s);

                return d;
            }

            float dstSwallow(float3 p, float s) {
                float d = dstS(p + float3(1. * s, 0, 0), s);
                d = min(d, dstW(p + float3(.75 * s, 0, 0), s));
                d = min(d, dstA(p + float3(.375 * s, 0, 0), s));
                d = min(d, dstL(p + float3(.05 * s, 0, 0), s));
                d = min(d, dstL(p - float3(.2, 0, 0), s));
                d = min(d, dstO(p - float3(.55, 0, 0), s));
                d = min(d, dstW(p - float3(.9, 0, 0), s));
                return d;
            }
            float dstSLove(float3 p, float s) {
                float d = dstS(p + float3(1.5 * s, 0, 0), s);
                d = min(d, dstK(p + float3(1.35 * s, 0, 0), s));
                d = min(d, dstI(p + float3(1.1 * s, 0, 0), s));
                d = min(d, dstN(p + float3(.95 * s, 0, 0), s));
                d = min(d, dstL(p + float3(.65 * s, 0, 0), s));
                d = min(d, dstE(p + float3(.4 * s, 0, 0), s));
                d = min(d, dstS(p + float3(.075 * s, 0, 0), s));
                d = min(d, dstS(p - float3(.1 * s, 0, 0), s));
                d = min(d, dstL(p - float3(.4 * s, 0, 0), s));
                d = min(d, dstO(p - float3(.75 * s, 0, 0), s));
                d = min(d, dstV(p - float3(1.1 * s, 0, 0), s));
                d = min(d, dstE(p - float3(1.35 * s, 0, 0), s));
                return d;
            }
            float dstMusic(float3 p, float s) {
                float d = dstM(p + float3(.6 * s, 0, 0), s);
                d = min(d, dstU(p + float3(.175 * s, 0, 0), s));
                d = min(d, dstS(p + float3(-.075 * s, 0, 0), s));
                d = min(d, dstI(p + float3(-.225 * s, 0, 0), s));
                d = min(d, dstC(p + float3(-.45 * s, 0, 0), s));
                d = min(d, dstSemiC(p + float3(-.55 * s, 0, 0), s));
                return d;
            }

            float dstXreg(float3 p, float s) {
                float d = dstX(p + float3(.4 * s, 0., 0.), s);
                d = min(d, dstR(p + float3(.15 * s, 0, 0), s));
                d = min(d, dstE(p - float3(.075 * s, 0, 0), s));
                d = min(d, dstG(p - float3(.45 * s, 0, 0), s));
                return d;
            }
            float dstCode(float3 p, float s) {
                float d = dstC(p + float3(.5 * s, 0., 0.), s);
                d = min(d, dstO(p + float3(.25 * s, 0., 0.), s));
                d = min(d, dstD(p - float3(.0, 0., 0.), s));
                d = min(d, dstE(p - float3(.25 * s, 0., 0.), s));
                d = min(d, dstSemiC(p - float3(.5 * s, 0., 0.), s));
                return d;
            }
            float3 TransformCode(float3 p) {
                p -= float3(0.5, .5, 0.);
                /*p.yz = mul(p.yz, rot2d(_Time.y * .79));
                p.xy = mul(p.xy, rot2d(_Time.y * .45));
                p.zx = mul(p.zx, rot2d(_Time.y * .35));*/
                return p;
            }
            float3 TransformXreg(float3 p) {
                p -= float3(0., .3, 0.);
                /*p.yz = mul(p.yz, rot2d(_Time.y * .49));
                p.xy = mul(p.xy, rot2d(_Time.y * .15));
                p.zx = mul(p.zx, rot2d(_Time.y * .65));*/
                return p;
            }

            float3 TransformMusic(float3 p)
            {
                p -= float3(-0.5, -.5, 0.);
                /*p.yz = mul(p.yz, rot2d(_Time.y * .29));
                p.xy = mul(p.xy, rot2d(_Time.y * .95));
                p.zx = mul(p.zx, rot2d(_Time.y * .33));*/
                return p;
            }

            float3 TransformSLove(float3 p)
            {
                p -= float3(0., -.3, 0.);
                /*p.yz *= rot2d(_Time.y * .59);
                p.xy *= rot2d(_Time.y * 1.25);
                p.zx *= rot2d(_Time.y * .44);*/
                return p;
            }


            float3 TransformSwallow(float3 p)
            {
                p -= float3(0., 0., 0.);
                /*p.yz *= rot2d(_Time.y * .79);
                p.xy *= rot2d(_Time.y * .25);
                p.zx *= rot2d(_Time.y * .25);*/
                return p;
            }

            RMHit GetDist(float3 p) {
                RMHit hit;
                float code = dstCode(TransformCode(p), 1.);
                float xreg = dstXreg(TransformXreg(p), 1.);
                float music = dstMusic(TransformMusic(p), 1.);
                float slove = dstSLove(TransformSLove(p), 1.);
                float swallow = dstSwallow(TransformSwallow(p), 1.);
                hit.d = smin(code, xreg, .1);
                hit.d = smin(hit.d, music, .1);
                hit.d = smin(hit.d, slove, .1);
                hit.d = smin(hit.d, swallow, .1);
                hit.id = 3;
                return hit;
            }

            RMHit RayMarch(float3 ro, float3 rd) {
                float dO = 0.;
                RMHit hit;
                for (int i = 0; i < MAX_STEPS; i++) {
                    float3 p = ro + rd * dO;
                    hit = GetDist(p);

                    dO += hit.d;
                    if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;
                }
                hit.d = dO;
                return hit;
            }

            float3 GetNormal(float3 p) {
                float d = GetDist(p).d;
                float2 e = float2(0.01, 0);

                float3 n = d - float3(
                    GetDist(p - e.xyy).d,
                    GetDist(p - e.yxy).d,
                    GetDist(p - e.yyx).d);
                return normalize(n);

            }

            float GetLight(float3 p, float3 lightPos) {

                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p);
                float dif = clamp(dot(n, l), 0., 1.);
                float d = RayMarch(p + n * SURF_DIST * 3., l).d;
                //if (d<length(lightPos-p)) dif *= .1; // shadow
                return dif;
            }



            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);            
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {                
                i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;                
                float3 col = 0;                
                float3 ro = float3(0, 0, -2);

                float3 rd = normalize(float3(uv.x, uv.y, 1));
                RMHit hit = RayMarch(ro, rd);

                if (hit.d < MAX_DIST) {
                    if ((hit.id == 3)) {
                        float3 p = ro + rd * hit.d;
                        float3 n = GetNormal(p);
                        float3 r = reflect(rd, n);
                        float spec = pow(max(0., r.y), 30.);
                        //if (hit.id == 1) BOX MAT
                        //if (hit.id == 2) SPHHERE MAT
                        float dif = dot(n, normalize(float3(1, 2, 3))) * .5 + .5;
                        //float l = GetLight(p, float3(sin(_Time.y) * 5.,0,cos(_Time.y) * 2.));
                        float l = GetLight(p, float3(0, 0, -1));
                        //float3 c = texture(iChannel0, p.xz).rgb;
                        col = lerp(1., l, .5) + spec;
                    }
                }
                return fixed4(col, 1);
            }
            ENDCG
        }
    }
}
