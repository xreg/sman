﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroMainCamera : SceneCameraBase
{

    public GameObject bgr;
    int fadeState = 0;
    Material mat = null;
    //private float timer = 0;
    private float amount = 0;
    void Start()
    {
        base.Start();
        mat = bgr.GetComponent<Renderer>().material;
        mat.SetFloat("_Amount", 0f);
            
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (fadeState == 0)
        {
            
            if (timer > 2)
            {
                fadeState++;
            }
        }
        if (fadeState == 1)
        {
            amount += Time.deltaTime / 2f;
            mat.SetFloat("_Amount", amount);
            if (timer > 4)
            {
                fadeState++;
            }
        }
        if ((fadeState == 2) && (timer > (SceneTimes.IntroScene - 1))){
            fadeState++;
        }
        if (timer > SceneTimes.IntroScene)
        {
            SceneManager.LoadScene("SecondScene");
        }
        if (fadeState == 3)
        {
            amount -= Time.deltaTime / 1f;
            mat.SetFloat("_Amount", amount);
        }
        
    }
}
