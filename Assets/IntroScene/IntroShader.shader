﻿Shader "IntroShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Amount("Amount",float) = 0
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag            
          
            #include "UnityCG.cginc"
            #include "../XCGInclude.cginc"
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
              
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
              
                return o;
            }
            #define MAX_STEPS 80
            #define MAX_DIST 10.
            #define SURF_DIST .001
            struct RMHit {
                float d;
                int id;
            };
            float fbm2(float2 p) {

                float freq = 1.;
                float amp = 1.;
                float v = 0.;
                for (int i = 0; i < 6; i++) {
                    float x = float(i);
                    float y = cos(i * 2.);
                    float2x2 R = float2x2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    v += amp * noise2d(x + y + mul(p, R) * freq);
                    freq *= 2.;
                    amp *= .5;
                }
                return v;
            }
            float Dollar(float3 p) {
                float d = dstCappedTorus(p - float3(0, .2, 0), float2(1., .0), .2, .05);
                float3 p2 = p + float3(0, .2, 0);
                p2.xy =mul(p2.xy, rot2d(3.14));
                d = smin(d, dstCappedTorus(p2, float2(1., 0.), .2, .05), .1);
                p2 = p;
                p2.y += 0.2;
                p2.xy = mul(p2.xy, rot2d(.8));
                d = smin(d, dstCappedTorus(p2, float2(.4, 0.3), .2, .05), .001);
                p2 = p;
                p2.y -= 0.2;
                p2.xy = mul(p2.xy, rot2d(4.14));
                d = smin(d, dstCappedTorus(p2, float2(.4, 0.3), .2, .05), .001);
                d = smin(d, dstVerticalCapsule(p + float3(0, .45, 0), .9, .04), .001);

                return d;
            }

            float Euro(float3 p) {
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(-1.5));
                float d = dstCappedTorus(p2, float2(1., .0), .2, .05);
                p2 = p;
                p2.x -= .25;
                p2.y -= .05;
                p2.xy = mul(p2.xy, rot2d(1.57));
                d = smin(d, dstVerticalCapsule(p2, .25, .04), .001);
                p2.x += .1;
                d = smin(d, dstVerticalCapsule(p2, .25, .04), .001);

                return d;

            }
            float3 TransformD(float3 p, float2 i_uv) {

                p.xy = mul(p.xy, rot2d(sin(_Time.y * i_uv.x)));
                p.xz = mul(p.xz,rot2d(cos(_Time.y * rand2d(i_uv))));
                p.yz = mul(p.yz,rot2d(sin(i_uv.x + _Time.y)));
                return p;
            }

            float3 TransformE(float3 p, float2 i_uv) {
                p.xy = mul(p.xy, rot2d(sin(_Time.y * i_uv.y)));
                p.xz = mul(p.xz, rot2d(sin(_Time.y * rand2d(i_uv * 432.2))));
                p.yz = mul(p.yz,rot2d(sin(_Time.y * rand2d(i_uv))));

                return p;
            }
            float3 TransformS(float3 p) {
                p.z += .1 + sin(_Time.y * 2.1) * .1;
                //p.xyz += _SPos.xyz;
                //p.xyz += _SPos.xyz;
                p.xy = mul(p.xy, rot2d(cos(_Time.y * 1.9) * .05));
                p.zx = mul(p.xz, rot2d(1.6 + sin(_Time.y * 1.5) * .1));
                //p.yz = mul(p.yz, rot2d(cos(_Time.y * .7)*.1)); 

                //p -= float3(0, 0, 0);
                return p;

            }

            float dstMan(float3 p, float s) {

                float speed = 10.;
                float beat = .5 * smoothstep(.0, 1., abs(sin(_Time.y * speed)));
                beat = 0.;
                //float beat = .5 * abs(sin(_Time.y * speed));
                float bbeat = .04 * sin(_Time.y * speed);
                bbeat = 0.;
                //float beat = abs(sin(_Time.y));
                float hbeat = 1. + .3 * sin(_Time.y * speed);
                float dbeat = 1. + .1 * cos(_Time.y * speed);
                float d = 0.;

                float3 a = float3(0., .25 * s, 0);
                float3 b = float3(0. + bbeat, -.7 * s, 0);

                float body = dstCapsule(p, a, b, .4 * s);


                a = float3(.25 * s, -.8 * s, 0);
                b = float3(.75 * s - beat, -1.5 * s, 0. + beat);
                float leg_r = dstCapsule(p, a, b, .25 * s);
                a = float3(-.25 * s, -.8 * s, 0);
                b = float3(-.75 * s + beat, -1.5 * s, 0. - beat);
                float leg_l = dstCapsule(p, a, b, .25 * s);

                a = float3(0., .25 * s, 0);
                b = float3(-1. * s, .25 * s - beat, 0. - beat);
                float arm_l = dstCapsule(p, a, b, .25 * s);
                a = float3(0., .25 * s, 0);
                b = float3(1. * s, .25 * s - beat, 0. - beat);
                float arm_r = dstCapsule(p, a, b, .25 * s);

                float dick = dstSphere(p - float3(0., -.85 * s, -.25 * s), .15 * s);
                float head = dstSphere(p - float3(0., .75 * s - beat * .2, 0), .5 * s + bbeat);

                float hat = dstCappedCylinder(p - float3(0, 1.3 * s, 0), .5 * s, .3 * s);
                float hat2 = dstCappedCylinder(p - float3(0, 1. * s, 0), .8 * s, .05 * s);

                a = p - float3(0, .73 * s, -.5 * s);
                a.yz = mul(a.yz, rot2d(2.));
                float nose = dstRoundCone(a, .06 * s, .03 * s, .2 * s);
                float eyel = dstSphere(p - float3(-0.2 * s, .8 * s, -.4 * s), .07 * s);
                float eyer = dstSphere(p - float3(0.2 * s, .8 * s, -.4 * s), .07 * s);

                float mouth = dstEllipsoid(p - float3(0, .55 * s, -.45 * s), float3(.16, .05, .1) * s);
                //float heart = Dollar(p + float3(-0.16 * s, 0, (0.3 * s) * hbeat), s * .3);
                //float euro = Euro(p + float3(0., .85 * s, (.4 * s) * dbeat), s * .4);
                d = body;
                d = smin(d, leg_l, .1 * s);
                d = smin(d, leg_r, .1 * s);
                d = smin(d, arm_l, .1 * s);
                d = smin(d, arm_r, .1 * s);

                d = smin(d, head, .1 * s);

                d = smin(d, dick, .1 * s);
                d = smin(d, hat, .1 * s);
                d = smin(d, hat2, .1 * s);
                d = smin(d, nose, .1 * s);
                d = smin(d, eyel, .01 * s);
                d = smin(d, eyer, .01 * s);
                d = smin(d, mouth, .1 * s);
                //d = smin(d, heart, .1 * s);
                //d = smin(d, euro, .1 * s);
                return d;

            }
            RMHit GetDist(float3 p, float2 i_uv) {

                RMHit hit;
                //float r = rand2d(i_uv);
                float dd = Dollar(TransformD(p, i_uv));
                float de = Euro(TransformE(p, i_uv));
                //float ds = dstMan(TransformS(p), .1);
               /* if (r < .7) {
                    float dd = Dollar(TransformD(p, i_uv));
                    float de = Euro(TransformE(p, i_uv));
                    
                    hit.d = smin(dd, de, .2);
                }
                else if (r >= .7 && r < .85) {
                    hit.d = Dollar(TransformD(p, i_uv));
                }
                else {
                    hit.d = Euro(TransformE(p, i_uv));
                }*/
                hit.d = lerp(dd, de, .5 + .5 + sin(_Time.y * .5 + rand2d(i_uv) * 3.14));
                hit.d -= fbm2(p.xz * 20) * 0.02;
                //float d2 = lerp(de, ds, .5 + .5 + sin(_Time.y * .5 + rand2d(i_uv) * 3.14));
                //hit.d = min(hit.d, d2);
                //hit.d = lerp(hit.d, ds, .5 + .5 + sin(_Time.y * .9 + rand2d(i_uv+42.79) * 3.14));
                hit.id = 1;
                return hit;
            }

            RMHit RayMarch(float3 ro, float3 rd, float2 i_uv) {
                float dO = 0.;
                RMHit hit;
                for (int i = 0; i < MAX_STEPS; i++) {
                    float3 p = ro + rd * dO;
                    hit = GetDist(p, i_uv);

                    dO += hit.d;
                    if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;

                }
                hit.d = dO;
                return hit;
            }

            float3 GetNormal(float3 p, float2 i_uv) {
                float d = GetDist(p, i_uv).d;

                float2 e = float2(0.01, 0);

                float3 n = d - float3(
                    GetDist(p - e.xyy, i_uv).d,
                    GetDist(p - e.yxy, i_uv).d,
                    GetDist(p - e.yyx, i_uv).d);
                return normalize(n);

            }

            float GetLight(float3 p, float3 lightPos, float2 i_uv) {

                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p, i_uv);
                float dif = clamp(dot(n, l), 0., 1.);
                float d = RayMarch(p + n * SURF_DIST * 3., l, i_uv).d;
                if (d < length(lightPos - p)) dif *= .1; // shadow
                return dif;
            }

            float3 GetRayDir(float2 uv, float3 p, float3 l, float z) {
                float3 f = normalize(l - p),
                    r = normalize(cross(float3(0, 1, 0), f)),
                    u = cross(f, r),
                    c = f * z,
                    i = c + uv.x * r + uv.y * u,
                    d = normalize(i);
                return d;
            }
            float3 xk7(float2 p) {
                float3 col = 0;
                float2 f_uv = frac(p);
                // X   

                col += smoothstep(0.025 + f_uv.y / 4., .05 + f_uv.y / 4., f_uv.x) * smoothstep(.075 + f_uv.y / 4., 0.05 + f_uv.y / 4., f_uv.x) * smoothstep(.95, .9, f_uv.y) * smoothstep(.05, .1, f_uv.y);
                col += smoothstep(0.275 - f_uv.y / 4., .3 - f_uv.y / 4., f_uv.x) * smoothstep(.325 - f_uv.y / 4., 0.3 - f_uv.y / 4., f_uv.x) * smoothstep(.95, .9, f_uv.y) * smoothstep(.05, .1, f_uv.y);
                // K

                col += smoothstep(0.325, 0.35, f_uv.x) * smoothstep(.375, .35, f_uv.x) * smoothstep(.95, .9, f_uv.y) * smoothstep(.05, .1, f_uv.y);;
                col += smoothstep(0.175 + f_uv.y / 3., .2 + f_uv.y / 3., f_uv.x) * smoothstep(.225 + f_uv.y / 3., 0.2 + f_uv.y / 3., f_uv.x) * smoothstep(.95, .9, f_uv.y) * smoothstep(.45, .5, f_uv.y);

                col += smoothstep(0.5 - f_uv.y / 3., .525 - f_uv.y / 3., f_uv.x) * smoothstep(.55 - f_uv.y / 3., 0.525 - f_uv.y / 3., f_uv.x) * smoothstep(.5, .45, f_uv.y) * smoothstep(.05, .1, f_uv.y);
                col += smoothstep(.55, .6, f_uv.x) * smoothstep(.85, .8, f_uv.x) * smoothstep(.95, .9, f_uv.y) * smoothstep(.8, .85, f_uv.y);

                col += smoothstep(0.55 + f_uv.y / 4., .6 + f_uv.y / 4., f_uv.x) * smoothstep(.65 + f_uv.y / 4., 0.6 + f_uv.y / 4., f_uv.x) * smoothstep(.95, .9, f_uv.y) * smoothstep(.05, .1, f_uv.y);
                return col;

            }
            fixed4 frag (v2f i) : SV_Target
            {
                fixed3 col = 0;
                i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
                
                float fx = 1. + smoothstep(.95, 1., abs(sin(_Time.y * 3.))) * 15.;// *sin(uv.y + _Time.y * 6.);
                float fy = 1. + smoothstep(.95, 1., cos(sin(_Time.y * 3.))) * 15.;// *cos(uv.y + _Time.y * 9.);

                float xo = .5 - (.5 / fx);
                float yo = .5 - (.5 / fy);
                float2 p = 0;

                col.r = length(uv) * .35;
                col.b = .5 - length(uv) * .35;
                float2 xuv = uv * (4. + 1. * sin(_Time.y * .9));
                xuv = mul(xuv, rot2d(_Time.y * .5));
                col -= xk7(float2(xuv.x * length(xuv), xuv.y * length(xuv.y))) * .08;
                uv =mul(uv, rot2d(sin(_Time.y * .2)));
                uv *= 1.; //+ 4. * sin(_Time.y*.6);                

                float3 ro = float3(0, 0, -1.);
                float2 f_uv = frac(uv) - .5;
                float2 i_uv = floor(uv);
                float xls = 1.;
                float yls = 1.;
                float NUM_L = 2.;


                float t = _Time.y * .025;
                t = .05;

                for (float i = 0.; i <= 1.; i += 1. / NUM_L) {
                    float d = frac(i + t);
                    float ss = lerp(4., .1, d);
                    float2 uv2 = uv * ss; //+ 4. * sin(_Time.y*.6);    	
                    uv2 += .5;

                    float2 f_uv = frac(uv2) - .5;
                    float2 i_uv = floor(uv2);
                    float dd = d * smoothstep(1., .94, d);

                    for (float y = -yls; y <= yls; y++) {
                        for (float x = -xls; x <= xls; x++) {
                            float2 off = float2(x, y);
                            float2 mv = 0;
                            mv.x = rand2d(i_uv + off);
                            mv.y = rand2d(i_uv + off);

                            float3 rd = GetRayDir(f_uv - off - mv * .9, ro, 0, 1.);

                            RMHit hit = RayMarch(ro, rd, i_uv + off);
                            if (hit.d < MAX_DIST) {
                                float3 p = ro + rd * hit.d;
                                //if (hit.id < 3) {
                                    float3 n = GetNormal(p, i_uv + off);
                                    float3 r = reflect(rd, n);
                                    float spec = pow(max(0., r.y), 20.);
                                    float dif = dot(n, normalize(float3(1, 2, 3))) * .5 + .5;
                                    //float l = GetLight(p, float3(0.,0,-2), i_uv);

                                    col += (lerp(float3(.8, 0.1, 1.), dif, .7) + spec) * dd;


                                //}

                            }
                          
                         /*   else {
                                col.rb += hit.d / 10.;
                            }*/
                        }
                    }
                }


                
                return fixed4(col, 1);
            }
            ENDCG
        }
        GrabPass { "_GrabTexture" }
                Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag


            #include "UnityCG.cginc"
             #include "../XCGInclude.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 grabUv : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _GrabTexture;
            float _Amount;
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.grabUv = ComputeGrabScreenPos(o.vertex);
                return o;
            }
       
            fixed4 frag(v2f i) : SV_Target
            {
                fixed3 col = 0;
                i.uv += .5;            
                
                
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
                float fx = 1. + smoothstep(.95, 1., abs(sin(_Time.y * 3.))) * 15.;// *sin(uv.y + _Time.y * 6.);
                float fy = 1. + smoothstep(.95, 1., cos(sin(_Time.y * 3.))) * 15.;// *cos(uv.y + _Time.y * 9.);
                
                float xo = .5 - (.5 / fx);
                float yo = .5 - (.5 / fy);
                float2 p = 0;
                
                p.x = xo + i.grabUv.x / fx;
                p.y = yo + i.grabUv.y / fy;
                i.grabUv.x = p.x;
                i.grabUv.y = p.y;
                //col += sin(uv.y*1.5)*.7;
                //col.r += cos(uv.y*1.+3.14);
                //col.b += sin(uv.x*3.14);
                

                col += tex2Dproj(_GrabTexture, i.grabUv).rgb;
                col += (1.-tex2D(_MainTex, i.uv-.5).rgb) * smoothstep(.9, 1., abs(sin(_Time.y*3)))*.5;
                col = lerp(0, col, _Amount);
                return fixed4(col, 1);
            }
        ENDCG
    }
    }
}
