﻿Shader "OldskoolPlasma"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Amount("Amount",float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
          

            #include "UnityCG.cginc"
            #include "../XCGInclude.cginc"
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
               
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Amount;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                
                return o;
            }
            #define PI 3.14159
            #define PI2 6.28
            float Star9(float2 p) {
                float l = length(p);
                float c = 0.04 / l;
                c *= smoothstep(.99999, 0.001, l);
                return c;

            }
            float3 txt9(float2 uv) {
                float3 col = 0;
                float2 tuv = uv + .5;

                uv *= 2.;
                //uv *= rot2d(_Time.y);
                float2 i_uv = floor(uv);
                float2 f_uv = frac(uv); // -.5;

                float2 p = f_uv;
                int ls = 1;
                float t = _Time.y * .5;
                float NUM_LAYERS = 6.;
                float NUM_DOTS = 4.;

                for (float z = 0.; z < 1.; z += 1. / NUM_LAYERS) {
                    float d = frac(t + z);
                    float stp = lerp(1., 0.0001, d);
                    float fadeout = d * smoothstep(.95, .1, d);

                    for (int y = -ls; y <= ls; y++) {
                        for (int x = -ls; x <= ls; x++) {
                            for (float i = 0.; i < 0.2; i += 0.1) {
                                float2 off = float2(x, y);
                                float a = rand2d(i_uv + off + z);

                                float2 rv = float2(.2 * sin(a * PI2), .2 * cos(a * PI2));
                                p = f_uv - off - rv;

                                float size = stp;

                                float s = Star9(p);
                                //float co = rand2d(i_uv + off);                
                                //col += s * size;// * smoothstep(0.5, 1., co);
                                float r = 1. - z;
                                float g = sin(z * PI2) * .5;
                                float b = cos(z * PI2);
                                col += lerp(float3(0., 0., 0.), float3(r, g, b), s * size);
                            }
                        }
                    }
                }
                return col;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                
                i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;                
                float3 col = 0;
                float2 st = float2(atan2(uv.x, uv.y), length(uv));
                
                //col.r = st.x / 6.28 +.5; //; -PI -- PI
                uv =mul(uv, rot2d(sin(_Time.y) * PI + cos(_Time.y * .34) * PI));
                float d = sqrt(uv.x * uv.x + uv.y * uv.y);
                float a = atan2(uv.y, uv.x);

                float u = 0.;
                float v = 0.;
                int MODE = int(fmod(floor(_Time.y * .5), 6.));
                if (MODE == 0) {

                    u = cos(a) / d; // semi tunnel
                    v = sin(a) / d;
                }
                else if (MODE == 1) {
                    u = uv.x / abs(uv.y); // floors
                    v = 1. / abs(uv.y);

                }
                else if (MODE == 2) {

                    u = d * cos(a + d); // plasmalike
                    v = d * sin(a + d);
                }
                else if (MODE == 3) {
                    u = 0.02 * uv.y + 0.03 * cos(a * 3.) / d; // whirl
                    v = 0.02 * uv.x + 0.03 * sin(a * 3.) / d;
                }
                else if (MODE == 4) {
                    u = uv.x * cos(2. * d) - uv.y * sin(12. * d/* * abs(14. * sin(_Time.y * .3))*/); // zoom whirl
                    v = uv.y * cos(2. * d) + uv.x * sin(2. * d);
                }
                else if (MODE == 5) {

                    u = .5 * uv.x / (.1 + d * .5);
                    v = .5 * uv.y / (.1 + d * .5);
                }

                float2 p = float2(u, v);
                float num_l = 10.;
                for (float i = 0.; i <= 1.; i += 1. / num_l) {
                    col += txt9(p + _Time.y * .1 + i * +(.1 + .18 * sin(_Time.y * .71))) / num_l;
                }
                col = lerp(0., col, _Amount);
                return fixed4(col, 1);
            }
            ENDCG
        }
    }
}
