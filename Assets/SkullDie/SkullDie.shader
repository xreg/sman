﻿Shader "SkullDie"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}       
        _Amount("Amount",float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }       
        GrabPass { "_GrabTexture" }
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "../XCGInclude.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 grabUv : TEXCOORD1;
                
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _GrabTexture;
            float _Amount;
#define PART_TWO  3
#define MAX_STEPS 50
#define MAX_DIST 10.
#define SURF_DIST .001
            struct RMHit {
                float d;
                int id;
                
            };
            float fbm(float2 p) {

                float freq = 1.3 + 1.2 * sin(_Time.y * .71);
                float amp = 1.0;
                float v = 0.;
                for (int i = 0; i < 6; i++) {
                    float x = float(i); // sin(_Time.y * 4);//float(i);
                    float y = cos(_Time.y * 3) * sin(_Time.y * 3.79);
                    //mat2 R = mat2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    float2x2 R = float2x2(cos(.5), -sin(.5), sin(.5), cos(.5));
                    p.xy *= mul(p, R);
                    v += amp * noise2d(x + y + p * freq);
                    freq *= 17.5 + 10.5 * sin(_Time.y * 1.37); // 2
                    amp *= .5; // .5
                }
                return v;
            }
            float fbm2(float2 p) {

                float freq = 1.;
                float amp = 1.;
                float v = 0.;
                for (int i = 0; i < 6; i++) {
                    float x = float(i);
                    float y = cos(i * 2.);
                    float2x2 R = float2x2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    v += amp * noise2d(x + y + mul(p, R) * freq);
                    freq *= 2.;
                    amp *= .5;
                }
                return v;
            }
            float Skull(float3 p) {
                float d = dstEllipsoid(p, float3(.2, .2, .2));
                float3 p2 = p - float3(.0, -0.18, -0.02);
                p2.yz = mul(p2.yz, rot2d(1.4));
                float chin = dstEllipsoid(p2, float3(.13, .18, .14));
                d = smin(d, chin, .1);
                float srem = dstEllipsoid(p - float3(0, -0.3, .2), float3(.4, .25, .3));
                d = smax(d, -srem, .1);
                float front = dstEllipsoid(p - float3(0, 0.15, 0), float3(.16, .1, .2));
                d = smin(front, d, .1);

                float hole = dstEllipsoid(p - float3(0.02, 0.1, 0.1), float3(.1, .1, .1));
                d = smax(d, -hole, .2);
                float lchin = dstEllipsoid(p - float3(0.2, 0.2, 0.), float3(.1, .1, .2));
                d = smax(-lchin, d, .1);


                float rchin = dstEllipsoid(p - float3(-0.2, -0.2, 0.), float3(.1, .1, .2));
                d = smax(-rchin, d, .1);

                float rc = dstEllipsoid(p - float3(0.1, -0.05, -0.03), float3(.1, .1, .1));
                d = smax(-rc, d, .1);


                float lc = dstEllipsoid(p - float3(-0.1, -0.05, -0.05), float3(.1, .1, .1));
                d = smax(-lc, d, .1);

                float eyel = dstEllipsoid(p + float3(-.06, -0.05, .1), float3(.05, .045, .1));
                d = smax(d, -eyel, .1);
                float eyer = dstEllipsoid(p + float3(+.06, -0.05, .1), float3(.05, .045, .1));
                d = smax(d, -eyer, .1);

                p2 = p + float3(0.014, .04, .2);
                p2.xy = mul(p2.xy, rot2d(.6));
                float nosel = dstEllipsoid(p2, float3(.015, .03, .2));
                d = smax(d, -nosel, .1);
                p2 = p + float3(-0.014, 0.04, .2);
                p2.xy = mul(p2.xy, rot2d(-.6));
                float noser = dstEllipsoid(p2, float3(.015, .03, .2));
                d = smax(d, -noser, .1);

                float t = dstRoundBox(p - float3(-.05, -.13, -0.2), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);
                t = dstRoundBox(p - float3(-.03, -.15, -0.15), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);
                t = dstRoundBox(p - float3(-.01, -.15, -0.1), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);
                t = dstRoundBox(p - float3(.01, -.15, -0.1), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);
                t = dstRoundBox(p - float3(.03, -.13, -0.15), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);
                t = dstRoundBox(p - float3(.05, -.15, -0.2), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.06);

                t = dstRoundBox(p - float3(-.05, -.19, -0.2), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);
                t = dstRoundBox(p - float3(-.03, -.19, -0.15), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);
                t = dstRoundBox(p - float3(-.01, -.19, -0.1), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);
                t = dstRoundBox(p - float3(.01, -.19, -0.1), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);
                t = dstRoundBox(p - float3(.03, -.19, -0.15), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);
                t = dstRoundBox(p - float3(.05, -.19, -0.2), float3(.0001, .005, .1), .001);
                d = smax(d, -t, 0.05);

                float brain = dstEllipsoid(p - float3(0, .1, 0), float3(.10, .10, .10));
                d = smax(d, -brain, .1);
                //d -= fbm(p.xz) * .01;

                float f = (fbm2(p.xz * 10.)) * 0.013;
                d -= f;
                f = (fbm2(p.xz * 7.)) * 0.01;
                d += f;
                return d;

            }

            float3 TransformS(float3 p) {
             //   p.z += .1 + sin(_Time.y * 2.1) * .1;               
                //p.xy = mul(p.xy, rot2d(cos(_Time.y * 1.9) * .05));

                return p;

            }
            float skull_talk(float3 p, float ta) {
                float d = dstEllipsoid(p, float3(.2, .2, .2));
                float3 p2 = p - float3(.0, -0.18 - ta, -0.02);
                p2.yz = mul(p2.yz, rot2d(1.4));
                float chin = dstEllipsoid(p2, float3(.13 - ta * .5, .18, .14));
                d = smin(d, chin, .1);
                float srem = dstEllipsoid(p - float3(0, -0.3, .2), float3(.4, .25, .3));
                d = smax(d, -srem, .1);
                float front = dstEllipsoid(p - float3(0, 0.15, 0), float3(.16, .1, .2));
                d = smin(front, d, .1);

                float hole = dstEllipsoid(p - float3(0.02, 0.1, 0.1), float3(.1, .1, .1));
                d = smax(d, -hole, .2);
                float lchin = dstEllipsoid(p - float3(0.2, 0.2, 0.), float3(.1, .1, .2));
                d = smax(-lchin, d, .1);


                float rchin = dstEllipsoid(p - float3(-0.2, -0.2, 0.), float3(.1, .1, .2));
                d = smax(-rchin, d, .1);

                float rc = dstEllipsoid(p - float3(0.1, -0.05, -0.03), float3(.1, .1, .1));
                d = smax(-rc, d, .1);


                float lc = dstEllipsoid(p - float3(-0.1, -0.05, -0.05), float3(.1, .1, .1));
                d = smax(-lc, d, .1);


                float eyel = dstEllipsoid(p + float3(-.06, -0.05, .1), float3(.05, .045, .1));
                d = smax(d, -eyel, .1);
                float eyer = dstEllipsoid(p + float3(+.06, -0.05, .1), float3(.05, .045, .1));
                d = smax(d, -eyer, .1);

                p2 = p + float3(0.014, .04, .2);
                p2.xy = mul(p2.xy, rot2d(.6));
                float nosel = dstEllipsoid(p2, float3(.015, .03, .2));
                d = smax(d, -nosel, .1);
                p2 = p + float3(-0.014, 0.04, .2);
                p2.xy = mul(p2.xy, rot2d(-.6));
                float noser = dstEllipsoid(p2, float3(.015, .03, .2));
                d = smax(d, -noser, .1);

                float cmouth2 = dstEllipsoid(p + float3(0.002, .17 + ta, 0.1), float3(.092, .05 + ta, .12));
                d = smax(d, -cmouth2, .001);

                float t = dstRoundBox(p - float3(-.05, -.165, -0.182), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.001);
                t = dstRoundBox(p - float3(-.03, -.165, -0.19), float3(.006, .01, .004), .001);
                d = smin(d, t, 0.001);
                t = dstRoundBox(p - float3(-.01, -.16, -0.195), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.005);
                t = dstRoundBox(p - float3(.01, -.16, -0.19), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.005);
                t = dstRoundBox(p - float3(.03, -.16, -0.19), float3(.006, .01, .004), .002);
                d = smin(d, t, 0.005);
                t = dstRoundBox(p - float3(.05, -.15, -0.19), float3(.008, .01, .004), .003);
                d = smin(d, t, 0.007);

                t = dstRoundBox(p - float3(.05, -.188 - ta, -0.19), float3(.005, .01, .004), .005);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(.03, -.19 - ta, -0.2), float3(.005, .01, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(.01, -.194 - ta, -0.2), float3(.004, .01, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(-.01, -.192 - ta, -0.21), float3(.004, .009, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(-.035, -.19 - ta, -0.2), float3(.004, .005, .004), .003);
                d = smin(d, t, 0.01);
                t = dstRoundBox(p - float3(-.055, -.19 - ta, -0.18), float3(.002, .003, .004), .003);
                d = smin(d, t, 0.01);

                float brain = dstEllipsoid(p - float3(0, .1, 0), float3(.10, .10, .10));
                float rr = 0.;
                if ((_Time.y - PART_TWO) > 0) rr = (_Time.y - PART_TWO) * .003;
                float f = (fbm2(p.xz * 10.)) * (0.01 + rr * .25);
                d -= f;
                f = (fbm2(p.xz * 7.)) * (0.007 + rr);
                d += f;
                return d;

            }

            RMHit GetDist(float3 p) {
                RMHit hit;
                float ta = 0.01;
                //if ((_Time.y > 2) && (_Time.y < PART_TWO)) ta = abs(sin(_Time.y * 6.)) * .013;

                float ds = skull_talk(TransformS(p), 0.01);

                hit.d = ds;
                hit.id = 1;

                return hit;
            }

            RMHit RayMarch(float3 ro, float3 rd) {
                float dO = 0.;
                RMHit hit;
                hit.d = 0;
                for (int i = 0; i < MAX_STEPS; i++) {
                    float3 p = ro + rd * dO;
                    hit = GetDist(p);

                    dO += hit.d;
                    if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;

                }
                hit.d = dO;
                return hit;
            }

            float3 GetNormal(float3 p) {
                float d = GetDist(p).d;

                float2 e = float2(0.01, 0);
                float3 n = d - float3(
                    GetDist(p - e.xyy).d,
                    GetDist(p - e.yxy).d,
                    GetDist(p - e.yyx).d);
                return normalize(n);

            }


            float GetLight(float3 p, float3 lightPos) {

                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p);
                float dif = clamp(dot(n, l), 0., 1.);
                float d = RayMarch(p + n * SURF_DIST * 3., l).d;
                //if (d < length(lightPos - p)) dif *= .1; // shadow
                return dif;
            }

            float3 GetRayDir(float2 uv, float3 p, float3 l, float z) {
                float3 f = normalize(l - p),
                    r = normalize(cross(float3(0, 1, 0), f)),
                    u = cross(f, r),
                    c = f * z,
                    i = c + uv.x * r + uv.y * u,
                    d = normalize(i);
                return d;
            }
            float pLine(float2 uv, float s) {
                float c = 0.;
                c = smoothstep(-.001 * s, .0, uv.y);
                c *= smoothstep(.001 * s, .0, uv.y);
                c *= smoothstep(-.5 * s, -.2 * s, uv.x);
                c *= smoothstep(.5 * s, .2 * s, uv.x);
                return c;
            }
            float pStar(float2 uv) {
                float num_s = 128;
                float c = 0.;
                for (float a = 0.; a < 1.; a += 1. / num_s) {
                    float2 uv2 = mul(uv, rot2d(a * 3.14159));
                    float s = .8 + .3 * step(0.5, abs(sin(a * 1.55 * num_s)));
                    c += pLine(uv2, s);
                }
                return c;
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.grabUv = ComputeGrabScreenPos(o.vertex);
                return o;
            }


            fixed4 frag (v2f i) : SV_Target
            {
               i.uv -= .5;
                    float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;
                    float2 uvR = uv;
                    float3 col = 0;

                    float2 uv2 = uv;
                    uv2 *= .5 + .2 * sin(_Time.y * .4);

                    uv2 = mul(uv2, rot2d(sin(_Time.y)));
                    col.r = pStar(uv2 + (float2(sin(_Time.y * 2), cos(_Time.y * .3)) * .01)) * .7;
                    col.g = pStar(uv2 * .4) * .01;
                    col.b = pStar(uv2 + (float2(sin(_Time.y * .17), cos(_Time.y * .8)) * .01) * .8) * .7;
                    uv.x += 1. * sin(_Time.y * .3);
                    uv.y += 1. * cos(_Time.y * .3);
                    float2 v = 0;
                    float2 w = 0;
                    v.x = fbm(uv + sin(_Time.y));
                    v.y = fbm(uv + v + cos(_Time.y));
                    w.x = fbm(uv + v.x);
                    w.y = fbm(uv + w.x);

                    float2 z = 0;
                    z.x = fbm(v.y + w.x);
                    z.y = fbm(v.x + w.y);
                    float2 p = 0;

                    float f = fbm(z.x + z.y + w.y + w.y);
                    f = f * f * f * f * f;
                    col.r += pStar(w);
                    col.b += pStar(v) * .7;
                    p.x = lerp(v.x, w.x, clamp(f, 0., 1.0));
                    p.y = lerp(v.y, w.y, clamp(f, 0., 1.));


                    float dx = p.x;
                    float dy = p.y;
                    col.b += clamp(f, 0., .2);
                    col.g += clamp(v.x, 0., .05);
                    col += tex2Dproj(_GrabTexture, i.grabUv).rgb;
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(dx, dy, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(dx, -dy, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(-dx, dy, 0, 0));
                    col += tex2Dproj(_GrabTexture, i.grabUv + float4(-dx, -dy, 0, 0));
                    col /= 5.4;
                    float3 ro = float3(0., 0., -1.);

                    float3 rd = GetRayDir(uvR, ro, 0, 1.);
                    rd = normalize(float3(uvR.x, uvR.y, 1.));
                    RMHit hit = RayMarch(ro, rd);

                    if (hit.d < MAX_DIST) {
                        float3 p = ro + rd * hit.d;
                        float3 n = GetNormal(p);
                        float3 r = reflect(rd, n);
                        float spec = pow(max(0., r.y), 10.);
                        float l = GetLight(p, float3(0, 0, -3));

                        float3 smat = float3(1., 1., 1.);

                        col = lerp(smat, l, .4) + spec;

                    }
                    col = lerp(0., col, _Amount);
                    return fixed4(col, 1);


            }
            ENDCG
        }
    }
}
