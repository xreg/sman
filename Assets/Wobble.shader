Shader "Wobble"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Amount("Amount",float) = 0

    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
           
            #include "UnityCG.cginc"
            #include "XCGInclude.cginc"
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Amount;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
               
                return o;
            }
#define MAX_STEPS 50
#define MAX_DIST 12.
#define SURF_DIST .001
#define PI 3.14159
            struct RMHit {
                float d;
                int id;
            };
            float fbm(float2 p) {

                float freq = 1. + .2 * sin(_Time.y * .547);
                float amp = 1. + .1 * sin(_Time.y * .747);;
                float v = 0.;
                for (int i = 0; i < 6; i++) {
                    float x = float(i);
                    float y = cos(_Time.y);
                    
                    float2x2 R = float2x2(cos(.5), sin(.5), -sin(.3), cos(.5));
                    v += amp * noise2d(x + y + mul(p, R) * freq);
                    freq *= 2.;
                    amp *= .5;
                }
                return v;
            }



            float3 gifLayer(float2 uv, float i) {

                i *= .2;
                float3 col = 0;
                float scale = 1.;
                float2 gv = frac(uv * scale) - .5; // center 0,0
                float2 i_uv = floor(uv * scale);

                float2 v = 0;
                float2 w = 0;
                v.x = fbm(uv);
                v.y = fbm(uv + v.x + sin(_Time.y * .23));
                w.x = fbm(uv + v.y + cos(_Time.y * .11));
                w.y = fbm(uv + w.x);
                float f = fbm(w + v);
                f = f * f * f * f;
                f *= sin(_Time.y * .734);
                float m = 0.;
                float ls = 1.;
                for (float y = -ls; y <= ls; y++) {
                    for (float x = -ls; x <= ls; x++) {
                        float2 off = float2(x, y);
                        float d = length(gv + off);
                        float dist = length(i_uv - off + f); // * sin(uv.x * 5.+_Time.y*.75) * cos(uv.y * 4.) ;
                        dist += sin((uv.x * uv.x) + (uv.y * uv.y) + _Time.y * i) * sin(_Time.y * i + uv.x * 10.);
                        dist += sin(uv.x * 2. + _Time.y * i) * cos(uv.y * 4. + _Time.y * i);
                        float r = lerp(.1, .6 + .5 * sin(_Time.y * .347), sin(-_Time.y + dist) * .5 + .5);
                        m = Xor(m, smoothstep(r, r * .5, d));
                        // m += smoothstep(r, r * .5, d);

                    }
                }
                //col = float3(m * .2);
                col = sin(float3(rand2d(i_uv + 24.24), 0., rand2d(i_uv))) * m * .6;

                return col;
            }

            float3 bgr(float2 uv) {
                float3 col = 0;

                uv.x += .1 * sin(_Time.y * .67);
                uv.y += .1 * sin(_Time.y * .567);
                float2 uv2 = uv;
                float num_l = 10.;
                for (float i = 0.; i <= 1.; i += 1. / num_l) {
                    float d = frac(i + _Time.y * .1);
                    float dd = lerp(10., .1, d);
                    float fade = d * smoothstep(1., .9, d);
                    uv2 = uv;

                    uv2 = mul(uv2, rot2d(i + _Time.y + rand2d(float2(i, i)) * sin(_Time.y * .789)));
                    float3 g = gifLayer(uv2 * dd + i * .69, i) * fade;
                    col += g;

                }

                return col;

            }


            float fbm2(float2 p) {

                float freq = 1.0;
                float amp = 1.0;
                float v = 0.;
                for (float i = 0.; i < 6.; i++) {
                    float x = _Time.y * .2;
                    float y = _Time.y * .5;
                    //mat2 R = mat2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    float2x2 R = float2x2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    v += amp * noise2d(x + y + mul(p,R) * freq);
                    freq *= 2.;
                    amp *= .5;
                }
                return v;
            }


            //float dstSphere(float3 p, float r) {
            //    float4 s = float4(0, 0, 0, r);
            //    float d = length(p - s.xyz) - s.w;
            //    return d;
            //}


            float dstPlane(float3 p) {
                float d = 0.;
                d = p.y + 1.;
                return d;
            }



            float wave(float2 uv) {                
                float w = 0.;
                float r = length(uv) ;
                uv *= 10.;
                float d = abs(sin(length(uv) * .3 - _Time.y));                
                d *= 1.4 / r;                
                d = smoothstep(.0, 1., d);
                w = d;
                return d;
            }

            RMHit GetDist(float3 p) {
                RMHit hit;
                hit.d = dstPlane(p);
                hit.d += wave(p.xz + float2(0, -1.)) * .2;
                hit.d += fbm2(p.xz) * .1;
                hit.d -= fbm2(p.xz * 10.) * .005;
                hit.d = smin(hit.d, dstSphere(p + float3(0, .6 + .4 * smoothstep(0.1, .9, abs(sin(_Time.y))), -1.), .5), 1.);
                hit.id = 3;
                return hit;
            }
            RMHit RayMarch(float3 ro, float3 rd) {
                float dO = 0.;
                RMHit hit;
                for (int i = 0; i < MAX_STEPS; i++) {
                    float3 p = ro + rd * dO;
                    hit = GetDist(p);

                    dO += hit.d;
                    if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;
                }
                hit.d = dO;
                return hit;
            }


            float3 GetNormal(float3 p) {
                float d = GetDist(p).d;
                float2 e = float2(0.01, 0);

                float3 n = d - float3(
                    GetDist(p - e.xyy).d,
                    GetDist(p - e.yxy).d,
                    GetDist(p - e.yyx).d);
                return normalize(n);

            }

            float GetLight(float3 p, float3 lightPos) {

                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p);
                float dif = clamp(dot(n, l), 0., 1.);
                float d = RayMarch(p + n * SURF_DIST * 3., l).d;
                if (d < length(lightPos - p)) dif *= .1; // shadow
                return dif;
            }


            fixed4 frag (v2f i) : SV_Target
            {
                i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;                
                float3 col = 0;
                
                float3 ro = float3(0, 0., -2);


                float zoom = 1.;
                float3 lookat = 0;

                lookat = float3(0., -1.5, 4.);
                float3 forward = normalize(lookat - ro);
                float3 right = cross(float3(0., 1., 0.), forward);
                float3 up = cross(forward, right);
                float3 center = ro + forward * zoom;
                float3 intersection = center + uv.x * right + uv.y * up;

                float3 rd = intersection - ro;

                //float3 rd = normalize(float3(uv.x,uv.y,1));
                RMHit hit = RayMarch(ro, rd);


                if (hit.d < MAX_DIST) {
                    float3 p = ro + rd * hit.d;
                    float3 n = GetNormal(p);
                    float3 r = reflect(rd, n);
                    float spec = pow(max(0., r.y), 30.);
                    //float dif = dot(n, normalize(float3(1,2,3)))*.5+.5;

                    float l = GetLight(p, float3(2. * sin(_Time.y * .79), 1., -4));
                    
                    col = lerp(bgr(p.xz),float3(l, l*.4, l), .8) + spec;
                    col -= hit.d / 50.;
                    if (hit.d > MAX_DIST - 4.) {
                        float f = (MAX_DIST - hit.d) * .25;
                        col = lerp(bgr(uv + float2(0, -.25)), col, f);
                    }

                }
                else if (uv.y > 0.) {
                    col += bgr(uv + float2(0, -.25));
                }
                col = lerp(0., col, _Amount);
                return fixed4(col, 1);
            }
            ENDCG
        }
    }
}
