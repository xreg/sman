﻿Shader "War"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
           

            #include "UnityCG.cginc"
            #include "../XCGInclude.cginc"
            
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;               
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
              
                return o;
            }
            #define MAX_STEPS 75
            #define MAX_DIST 8.
            #define SURF_DIST .001
            struct RMHit {
                float d;
                int id;
            };
            float fbm(float2 p) {

                float freq = 1.;
                float amp = 1.0;
                float v = 0.;
                for (float i = 0.; i < 6.; i++) {
                    float x = i * i;
                    float y = i;
                    //mat2 R = mat2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    float2x2 R = float2x2(cos(.5), sin(.5), -sin(.5), cos(.5));
                    v += amp * noise2d(x + y + mul(p,R) * freq);
                    freq *= 2.;
                    amp *= .5;
                }
                return v;
            }

            float Star(float2 p) {
                float l = length(p);
                float c = 0.05 / l;
                c *= smoothstep(1., 0.0, l);
                return c;
            }

            float Dollar(float3 p, float s) {
                float d = dstCappedTorus(p - float3(0, .2 * s, 0), float2(1. * s, .0), .2 * s, .05 * s);
                float3 p2 = p + float3(0, .2 * s, 0);
                p2.xy = mul(p2.xy, rot2d(3.14));
                d = smin(d, dstCappedTorus(p2, float2(1. * s, 0.), .2 * s, .05 * s), .1 * s);
                p2 = p;
                p2.y += 0.2 * s;
                p2.xy = mul(p2.xy, rot2d(.8));
                d = smin(d, dstCappedTorus(p2, float2(.4 * s, 0.3 * s), .2 * s, .05 * s), .001 * s);
                p2 = p;
                p2.y -= 0.2 * s;
                p2.xy = mul(p2.xy, rot2d(4.14));
                d = smin(d, dstCappedTorus(p2, float2(.4 * s, 0.3 * s), .2 * s, .05 * s), .001 * s);
                d = smin(d, dstVerticalCapsule(p + float3(0, .45 * s, 0), .9 * s, .04 * s), .001 * s);
                return d;
            }
            float dst_w(float3 p, float s) {
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(1.2));
                float d = dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s);
                p2 = p;
                p2 -= float3(.2 * s, 0., 0.);
                p2.xy = mul(p2.xy, rot2d(1.2));
                d = smin(d, dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                p2 = p - float3(.1 * s, 0., 0.);
                p2.xy = mul(p2.xy, rot2d(-1.2));
                d = smin(d, dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                p2 = p;
                p2 -= float3(.3 * s, 0., 0.);
                p2.xy = mul(p2.xy, rot2d(-1.2));
                d = smin(d, dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                return d;
            }

            float dst_a(float3 p, float s) {
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(-1.2));
                float d = dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s);
                p2 = p - float3(.12 * s, 0., 0.);
                p2.xy = mul(p2.xy, rot2d(1.2));
                d = smin(d, dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                d = smin(d, dstRoundBox(p + float3(-0.06, 0.025 * s, 0.), float3(.06 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                return d;
            }

            float dst_r(float3 p, float s) {
                float3 p2 = p;
                p2.xy = mul(p2.xy, rot2d(-1.56));
                float d = dstRoundBox(p2, float3(.16 * s, .002 * s, .05 * s), 0.02 * s);

                d = smin(d, dstRoundBox(p + float3(-.08 * s, -.158 * s, 0), float3(.05 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                d = smin(d, dstRoundBox(p + float3(-.08 * s, -.01 * s, 0), float3(.05 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                p2 = p + float3(-.15 * s, -.08 * s, 0);
                p2.xy = mul(p2.xy, rot2d(-1.56));
                d = smin(d, dstRoundBox(p2, float3(.07 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);
                p2 = p + float3(-.12 * s, .08 * s, 0);
                p2.xy = mul(p2.xy, rot2d(-1.2));
                d = smin(d, dstRoundBox(p2, float3(.095 * s, .002 * s, .05 * s), 0.02 * s), .01 * s);

                return d;
            }
            float war(float3 p, float s) {
                float3 p2 = p + float3(.5 * s, 0., 0);
                p2.yz = mul(p2.yz, rot2d(3.14159));
                float d = dst_w(p2, s);
                p2 = p;
                p2.yz = mul(p2.yz, rot2d(3.14159));
                d = smin(d, dst_a(p2, s), .01 * s);
                d = smin(d, dst_r(p - float3(.35 * s, 0., 0.), s), .01 * s);
                float f = (fbm(p.xz * 10.)) * 0.009;
                d -= f;
                f = (fbm(p.xz * 7.)) * 0.01;
                d += f;
                return d;
            }
            float wobble(float3 p) {
                float d = 0.;
                float3 p2 = p;
                p2.z += _Time.y * .4;
                p2.x += _Time.y * .2;
                p.x += sin(2.54 + fbm(p2.yz) * 5.5) * .1;
                p.z += sin(1.5 + fbm(p2.xy) * 3.1) * .04;
                p.y += sin(_Time.y * 2.233 + fbm(p2.zx) * 6.4) * .04;
                //d = dstSphere(p, 1.);
                d = p.y + .4;
                d += fbm(p.xz) * .1;
                d += fbm(p.zy * 5.) * .05;
                return d;

            }
            float3 TransformW(float3 p) {
                p.y += .35;
                p.z -= .1;
                p.xy = mul(p.xy, rot2d(sin(_Time.y * 5.8) * .05));
                p.xz = mul(p.xz, rot2d(cos(_Time.y * 4.) * .1));
                p.yz = mul(p.yz, rot2d(1. + sin(_Time.y * 4.7) * .1));

                
                return p;

            }
            float3 TransformDollar(float3 p) {
                p.y += .4;
                p.z -= 1. + abs(sin(_Time.y)) * .2;
                p.xy = mul(p.xy, rot2d(3.14159 + sin(_Time.y * 5.8) * .2));
                p.xz = mul(p.xz, rot2d(cos(_Time.y * 4.) * .2));
                p.yz = mul(p.yz, rot2d(1.6 + sin(_Time.y * 4.7) * .1));

                
                return p;

            }
            float3 TransformWo(float3 p) {
                p.z -= 1.;
                return p;
            }

            RMHit GetDist(float3 p) {
                RMHit hit;

                hit.d = wobble(TransformWo(p));
                hit.d = smin(hit.d, Dollar(TransformDollar(p), 1.), .25);
                hit.d = smin(hit.d, war(TransformW(p), 1.), .45);

                hit.id = 1;
                return hit;
            }

            RMHit RayMarch(float3 ro, float3 rd) {
                float dO = 0.;
                RMHit hit;
                for (int i = 0; i < MAX_STEPS; i++) {
                    float3 p = ro + rd * dO;
                    hit = GetDist(p);

                    dO += hit.d;
                    if ((dO > MAX_DIST) || (hit.d < SURF_DIST)) break;

                }
                hit.d = dO;
                return hit;
            }

            float3 GetNormal(float3 p) {
                float d = GetDist(p).d;

                float2 e = float2(0.01, 0);

                float3 n = d - float3(
                    GetDist(p - e.xyy).d,
                    GetDist(p - e.yxy).d,
                    GetDist(p - e.yyx).d);
                return normalize(n);

            }

            float GetLight(float3 p, float3 lightPos) {

                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p);
                float dif = clamp(dot(n, l), 0., 1.);
                float d = RayMarch(p + n * SURF_DIST * 3., l).d;
                //if (d<length(lightPos-p)) dif *= .1; // shadow
                return dif;
            }
            float3 Bg2(float2 uv) {
                uv = mul(uv, rot2d(_Time.y * .1));
                float2 o_uv = uv;


                float2 i_uv = floor(uv);
                float2 f_uv = frac(uv);

                float2 fbm_uv = uv * 1.;

                float3 col = 0;
                float2 v = 0;
                float2 w = 0;
                float2 p = v * .2;
                int ls = 1;
                float NUM_L = 4.;
                float t = _Time.y * .1;
                for (float i = 0.; i < 1.; i += 1. / NUM_L) {
                    for (int y = -ls; y <= ls; y++) {
                        for (int x = -ls; x <= ls; x++) {
                            float d = frac(t + i);
                            float s = lerp(10., 1., d); // movement
                            float dd = d * smoothstep(1., .9, d);  // fadeout          
                            fbm_uv = uv * s;
                            float2 off = float2(float(x), float(y));
                            v.x = fbm(i_uv + fbm_uv + off + rand2d(i_uv + off));
                            v.y = fbm(i_uv + fbm_uv + v.x + off + rand2d(i_uv + off + float2(.3, 1.79)));
                            w.x = fbm(i_uv + fbm_uv + v.x + off + rand2d(i_uv + off + float2(6.6, 9.89)));
                            w.y = fbm(i_uv + fbm_uv + w.x + off + rand2d(i_uv + off));

                            float2 mv = 0; // "dot" movement            
                            mv.x = .3 * rand2d(i_uv + off + i) * cos(_Time.y);
                            mv.y = .4 * rand2d(i_uv + off) * sin(_Time.y * .79);

                            col.r += Star(f_uv - off - w * (d * .8) - mv) * dd;
                            col.b += lerp(0., .034, col.r) * dd;
                        }
                    }
                }
                return col;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                i.uv -= .5;
                float2 uv = (i.uv * _ScreenParams.xy) / _ScreenParams.y;                
                float3 col = 0;
                float3 ro = float3(0, 0, -1);
                float3 rd = normalize(float3(uv.x, uv.y, 1.));

                RMHit hit = RayMarch(ro, rd);

                if (hit.d < MAX_DIST) {
                    float3 p = ro + rd * hit.d;
                    float3 n = GetNormal(p);
                    float3 r = reflect(rd, n);
                    float spec = pow(max(0., r.y), 30.);
                    //float dif = dot(n, normalize(float3(1,2,3)))*.5+.5;

                    float l = GetLight(p, float3(0, 1., -5));

                    col = lerp(Bg2(p.xz), l, .0) + spec;
                    col -= hit.d / 80.;
                    if (hit.d > MAX_DIST - 4.) {
                        float f = (MAX_DIST - hit.d) * .25;
                        col = lerp(Bg2(uv), col, f);
                    }

                }
                else if (uv.y > -.3) {
                    col += Bg2(uv);
                }

               
                return fixed4(col, 1);
            }
            ENDCG
        }
    }
}
