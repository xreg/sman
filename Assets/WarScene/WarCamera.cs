﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WarCamera : SceneCameraBase
{
    // Start is called before the first frame update
    void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > SceneTimes.BlackSunScene)
        {
            SceneManager.LoadScene("OldSkoolPlasmaScene");
        }
    }
}
