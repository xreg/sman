#ifndef X_CG_INCLUDE
#define X_CG_INCLUDE

#define PI 3.14159
#define PI2 6.28


float rand1d(float p) {
    float r = frac(sin(dot(p, 756.34)) * 729342.);
    return r;
}

float2 rand2dv(float2 p) {
    float2 v = 0;
    v.x = rand1d(p.x);
    v.y = rand1d(p.y);
    return v;
}

float rand2d(float2 p) {
		float r = frac(sin(dot(p, float2(3421.32, 756.34))) * 423742.97);
		return r;
}

float rand3d(float3 p) {
	float r = frac(sin(dot(p, float3(3421.732, 1276.34, 5431.567))) * 4282.297);
	return r;

}


float2x2 rot2d(float a) {
    return float2x2(cos(a), -sin(a),
        sin(a), cos(a));
}

float noise2d(float2 p) {

	float2 i_p = floor(p);
	float2 f_p = frac(p);
	float a = rand2d(i_p);
	float b = rand2d(i_p + float2(1., 0.));
	float c = rand2d(i_p + float2(0., 1.));
	float d = rand2d(i_p + float2(1., 1.));
	float2 u = smoothstep(0., 1., f_p);
	return lerp(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
}
float noise3d(float3 p) {
	float3 i_p = floor(p);
	float3 f_p = frac(p);
	float a = rand3d(i_p);
	float b = rand3d(i_p + float3(1., 0., 0.));
	float c = rand3d(i_p + float3(0., 1., 0.));
	float d = rand3d(i_p + float3(1., 1., 0.));
	float e = rand3d(i_p + float3(0., 0., 1.));
	float f = rand3d(i_p + float3(1., 0., 1.));
	float g = rand3d(i_p + float3(0., 1., 1.));
	float h = rand3d(i_p + float3(1., 1., 1.));
	float3 u = smoothstep(0., 1., f_p);

	float f1 = lerp(a, b, u.x);
	float f2 = lerp(c, d, u.x);
	float b1 = lerp(e, f, u.x);
	float b2 = lerp(g, h, u.x);
	float d1 = lerp(f1, f2, u.y);
	float d2 = lerp(b1, b2, u.y);
	float n = lerp(d1, d2, u.z);

	return n;

}

float smin(float a, float b, float k) {
	float h = clamp(0.5 + 0.5 * (b - a) / k, 0., 1.);
	return lerp(b, a, h) - k * h * (1.0 - h);
}

float smax(float a, float b, float k) {
	float h = clamp((b - a) / k + .5, 0., 1.);
	return lerp(a, b, h) + h * (1. - h) * k * .5;
}
float Xor(float a, float b) {
	return a * (1. - b) + b * (1. - a);
}

// dst functions
float dstPlane(float3 p, float3 n, float h)
{
	// n must be normalized
	return dot(p, n) + h;
}
float dstBox(float3 p, float3 s) {
	p = abs(p) - s;
	return length(max(p, 0.)) + min(max(p.x, max(p.y, p.z)), 0.);
}

float dstSphere(float3 p, float r) {
	float4 s = float4(0, 0, 0, r);
	float d = length(p - s.xyz) - s.w;
	return d;
}


float dstRoundedCylinder(float3 p, float ra, float rb, float h)
{
	float2 d = float2(length(p.xz) - 2.0 * ra + rb, abs(p.y) - h);
	return min(max(d.x, d.y), 0.0) + length(max(d, 0.0)) - rb;
}
float dstRoundBox(float3 p, float3 b, float r)
{
	float3 q = abs(p) - b;
	return length(max(q, 0.0)) + min(max(q.x, max(q.y, q.z)), 0.0) - r;
}

float dstVerticalCapsule(float3 p, float h, float r)
{
	p.y -= clamp(p.y, 0.0, h);
	return length(p) - r;
}
float dstCapsule(float3 p, float3 a, float3 b, float r)
{
	float3 pa = p - a, ba = b - a;
	float h = clamp(dot(pa, ba) / dot(ba, ba), 0.0, 1.0);
	return length(pa - ba * h) - r;
}

float dstCappedTorus(in float3 p, in float2 sc, in float ra, in float rb)
{
	p.x = abs(p.x);
	float k = (sc.y * p.x > sc.x * p.y) ? dot(p.xy, sc) : length(p.xy);
	return sqrt(dot(p, p) + ra * ra - 2.0 * ra * k) - rb;
}
float dstTorus(float3 p, float2 t)
{
	float2 q = float2(length(p.xz) - t.x, p.y);
	return length(q) - t.y;
}
float dstCappedCylinder(float3 p, float h, float r)
{
	float2 d = abs(float2(length(p.xz), p.y)) - float2(h, r);
	return min(max(d.x, d.y), 0.0) + length(max(d, 0.0));
}
float dstEllipsoid(float3 p, float3 r)
{
	float k0 = length(p / r);
	float k1 = length(p / (r * r));
	return k0 * (k0 - 1.0) / k1;
}

float dstRoundCone(float3 p, float r1, float r2, float h)
{
	float2 q = float2(length(p.xz), p.y);

	float b = (r1 - r2) / h;
	float a = sqrt(1.0 - b * b);
	float k = dot(q, float2(-b, a));

	if (k < 0.0) return length(q) - r1;
	if (k > a * h) return length(q - float2(0.0, h)) - r2;

	return dot(q, float2(a, b)) - r1;
}

#endif